import { configure } from '@storybook/react';
import { setDefaults } from '@storybook/addon-info'

const req = require.context('../src/components', true, /\.stories\.js$/)

setDefaults({
  inline: true,
  header: false
})

function loadStories() {
  req.keys().forEach((filename) => req(filename))
}

configure(loadStories, module);
