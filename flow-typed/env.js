declare var process: {
  env: {
    API_PORT: number,
    API_HOST: string,
    NODE_ENV: string,
    PORT: number
  }
}
