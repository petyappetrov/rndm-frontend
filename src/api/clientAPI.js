// @flow

import axios from 'axios'
import { first, reduce } from 'lodash'
import ContestAPI from './contestAPI'
import UserAPI from './userAPI'


const API_HOST = process.env.API_HOST || 'http://localhost'
const API_PORT = process.env.API_PORT || '8888'


class API {
  request: Function
  createFormData: Function
  constructor () {
    this.request = axios.create({
      baseURL: `${API_HOST}:${API_PORT}/api`,
      headers: {
        'Content-Type': 'application/json'
      }
    })

    this.createFormData = (values) =>
      reduce(values, (data, value, key) => {
        if (first(value) instanceof Blob) {
          data.append(key, first(value))
        } else {
          data.append(key, value)
        }
        return data
      }, new FormData())
  }

  setToken = (token: string) =>
    this.request.defaults.headers.Authorization = `Bearer ${token}`
}


class ClientAPI extends API {
  contest: {
    createContest: Function,
    getContests: Function,
    removeContest: Function
  }

  user: {
    getUser: Function,
    updateUser: Function
  }

  constructor () {
    super()
    this.contest = new ContestAPI(this)
    this.user = new UserAPI(this)
  }

  signin = (payload: Object) =>
    this.request({
      method: 'POST',
      url: 'auth/signin',
      data: payload
    })

  signup = (payload: Object) =>
    this.request({
      method: 'POST',
      url: 'auth/signup',
      data: payload
    })
}


export default new ClientAPI()
