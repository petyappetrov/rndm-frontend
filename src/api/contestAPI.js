class ContestAPI {
  constructor ({ request, createFormData }) {
    this.request = request
    this.createFormData = createFormData
  }

  getContests = (params) =>
    this.request({
      method: 'GET',
      url: 'getContests',
      params
    })

  createContest = ({ payload }) =>
    this.request({
      method: 'POST',
      url: 'createContest',
      data: this.createFormData(payload),
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })

  removeContest = (id) =>
    this.request({
      method: 'POST',
      url: 'removeContest',
      data: { id }
    })
}


export default ContestAPI
