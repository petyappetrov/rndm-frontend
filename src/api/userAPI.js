class UserAPI {
  constructor ({ request, createFormData }) {
    this.request = request
    this.createFormData = createFormData
  }

  getUser = (id) =>
    this.request({
      method: 'GET',
      url: 'getUser',
      params: {
        id
      }
    })

  updateUser = (values: Object) =>
    this.request({
      method: 'POST',
      url: 'updateUser',
      data: this.createFormData(values),
      headers: {
        'Content-Type': 'multipart/form-data'
      }
    })

  removeUser = (id) =>
    this.request({
      method: 'POST',
      url: 'removeUser',
      body: {
        id
      }
    })
}

export default UserAPI

