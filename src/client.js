import 'babel-polyfill'

import { AppContainer } from 'react-hot-loader'
import * as React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import moment from 'moment'
import { Router, Route } from 'react-router-dom'
import { has } from 'lodash'
import { ThemeProvider } from 'styled-components'
import { history, loadPersistedState } from 'utils'
import { whyDidYouUpdate } from 'why-did-you-update'
import Application from 'containers/Application'
import configureStore from 'redux/configureStore'
import clientAPI from 'api/clientAPI'


/**
 * Styles of plugins
 */
import 'react-day-picker/lib/style.css'
import 'react-select/dist/react-select.css'
import 'rc-time-picker/assets/index.css'


/**
 * Global styles
 */
import './globalStyles'


/**
 * For optimizing to re-render component
 */
if (process.env.NODE_ENV === 'development' && process.env.OPTIMIZE) {
  whyDidYouUpdate(React)
}


/**
 * Setting global localization of date formats
 */
moment.locale('ru')


/**
 * Load token from localStorage
 */
const persistedState = loadPersistedState()


/**
 * Save global authorization token in axios
 */
if (has(persistedState, 'auth.token')) {
  clientAPI.setToken(persistedState.auth.token)
}


/**
 * Configure redux store
 */
const store = configureStore(persistedState)


/**
 * Render application to DOM
 */
const render = Component =>
  ReactDOM.render(
    <Provider store={store}>
      <ThemeProvider
        theme={{
          space: [0, 6, 12, 18, 24],
          breakpoints: [32, 48, 64],
          shadow: '0 1px 2px rgba(25, 25, 25, 0.2)'
        }}
      >
        <AppContainer>
          <Router history={history}>
            <Route component={Component} />
          </Router>
        </AppContainer>
      </ThemeProvider>
    </Provider>,
    document.getElementById('root')
  )

render(Application)


/**
 * Hot reload to project after changes
 */
if (module.hot) {
  module.hot.accept('containers/Application', () => render(Application))
}
