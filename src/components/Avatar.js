import React from 'react'
import styled from 'styled-components'
import { pure } from 'recompose'
import { ImageLoader } from 'components'

const Avatar = styled.div`
  width: 150px;
  height: 150px;
  border-radius: 50%;
  overflow: hidden;
  margin-top: 10px;
`

export default pure(({ src }) =>
  <Avatar>
    <ImageLoader src={src} width={150} height={150} />
  </Avatar>
)
