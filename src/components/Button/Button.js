// @flow

import * as React from 'react'
import styled, { css } from 'styled-components'
import { space, width, responsiveStyle } from 'styled-system'
import { renderComponent, branch, setDisplayName, compose } from 'recompose'
import { Link } from 'react-router-dom'

const order = responsiveStyle('order')

const views = {
  default: `
    background-color: #3193DF;
    color: #fff;
    &:hover {
      background-color: #45a1e9;
    }
  `,
  primary: `
    background-color: #23C790;
    color: #fff;
    &:hover {
      background-color: #26D399;
    }
  `,
  ghost: `
    background-color: transparent;
    color: #3193DF;
    border: 1px solid #3193DF;
    line-height: 34px;
    &:hover {
      background-color: #45a1e9;
      border-color: #45a1e9;
      color: #fff;
    }
  `,
  other: `
    background-color: transparent;
    color: #fff;
    border: 1px solid #fff;
    line-height: 34px;
    &:hover {
      background-color: #fff;
      color: #000;
    }
  `,
  danger: `
    background-color: #CD0F21;
    color: #fff;
    &:hover {
      background-color: #e43848;
    }
  `
}

const buttonCSS = css`
  ${space}
  ${width}
  ${order}
  display: inline-block;
  height: 36px;
  line-height: 36px;
  font-size: 1rem;
  border-radius: 18px;
  box-sizing: border-box;
  border: none;
  cursor: pointer;
  padding: 0 20px;
  user-select: none;
  &:disabled {
    opacity: 0.55;
    pointer-events: none;
    cursor: default;
  }
  ${(p) => views[p.view]}
`

const ButtonStyled = styled.button`
  ${buttonCSS}
`

const LinkStyled = styled(Link)`
  ${buttonCSS}
`

type Props = {
  view?: 'default' | 'primary' | 'danger' | 'other' | 'ghost',
  type?: string,
  to?: string,
  children: React.Node
}

const enhance = compose(
  setDisplayName('Button'),
  branch(
    props => props.to,
    renderComponent(LinkStyled)
  )
)

const Button = enhance(({
  view = 'default',
  type = 'button',
  children,
  ...props
}: Props) =>
  <ButtonStyled
    view={view}
    type={type}
    {...props}
  >
    {children}
  </ButtonStyled>
)

export default Button
