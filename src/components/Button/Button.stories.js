import React from 'react'
import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { withInfo } from '@storybook/addon-info'
import Button from './Button'


storiesOf('Button', module)
  .add('Default Button', withInfo({
    text: 'The default button is used everywhere, for links, for button, for toggle'
  })(() => (
    <Button onClick={action('clicked')}>Hello Button</Button>
  )))
  .add('Primary Button', withInfo({
    text: 'The default button is used everywhere, for links, for button, for toggle'
  })(() => (
    <Button view='primary' onClick={action('clicked')}>Hello Button</Button>
  )))
  .add('Ghost Button', withInfo({
    text: 'The default button is used everywhere, for links, for button, for toggle'
  })(() => (
    <Button view='ghost' onClick={action('clicked')}>Hello Button</Button>
  )))
  .add('Danger Button', withInfo({
    text: 'The default button is used everywhere, for links, for button, for toggle'
  })(() => (
    <Button view='danger' onClick={action('clicked')}>Hello Button</Button>
  )))
  .add('Other Button', withInfo({
    text: 'The default button is used everywhere, for links, for button, for toggle'
  })(() => (
    <Button view='other' onClick={action('clicked')}>Hello Button</Button>
  )))
