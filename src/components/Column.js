import * as React from 'react'
import { Box } from 'grid-styled'
import styled from 'styled-components'

const Column = styled(props => <Box px='7.5px' flex='0 0 auto' {...props} />)`
  ${p => p.text && `
    text-align: ${p.text}
  `}
`

export default Column
