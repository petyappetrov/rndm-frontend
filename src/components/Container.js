import { Box } from 'grid-styled'
import styled from 'styled-components'


const Container = styled(Box)`
  margin: 0 auto;
  max-width: 80rem;
  padding: 0 2rem;
  flex-grow: 1;
  @media (max-width: 32rem) {
    padding: 0 1rem;
  }
`

export default Container
