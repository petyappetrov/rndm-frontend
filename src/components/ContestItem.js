// @flow

import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import moment from 'moment'
import { sliceString } from 'utils'
import { ImageLoader } from 'components'

const ContestItem = styled(Link)`
  width: 100%;
  border-radius: 4px;
  overflow: hidden;
  background: #fff;
  box-sizing: border-box;
  display: block;
  color: #000;
  height: auto;
  box-shadow: ${p => p.theme.shadow};
  &:hover {
    text-decoration: none;
    box-shadow: 0 0 0 1px #45a1e9,
                0 0 0 3px rgba(0, 126, 255, 0.1);
  }
`

const ContestImage = styled.div`
  height: auto;
  overflow: hidden;
  > img {
    object-fit: cover;
    width: 100%;
    height: 100%;
  }
`

const ContestContent = styled.div`
  padding: 15px;
`

const ContestTitle = styled.div`
  font-size: 22px;
  font-weight: 700;
  text-transform: uppercase;
  margin: 0 0 10px;
  word-wrap: break-word;
`

const ContestFinish = styled.div`
  &:first-letter {
    text-transform: uppercase;
  }
`

type Props = {
  contest: Object
}


export default ({ contest }: Props) =>
  <ContestItem to={`/contests/${contest._id}`}>
    <ContestImage>
      <ImageLoader src={contest.thumbnailUrl} alt={contest.title} />
    </ContestImage>
    <ContestContent>
      <ContestTitle>{sliceString(contest.title, 60)}</ContestTitle>
      <ContestFinish>
        Закончится {moment(contest.endDate).format('D MMMM, YYYY')}
        <div>
          {contest.type}
        </div>
        <div>
          {contest.city}
        </div>
      </ContestFinish>
    </ContestContent>
  </ContestItem>
