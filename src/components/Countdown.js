// @flow

import * as React from 'react'
import styled from 'styled-components'
import moment from 'moment'

const CountdownStyled = styled.div`
  display: flex;
  justify-content: flex-start;
`

const Col = styled.div`
  height: 60px;
  width: 60px;
  border: 2px solid #000;
  border-radius: 8px;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin: 0 15px 0 0;
`

const ColValue = styled.div`
  font-size: 22px;
`

const ColLabel = styled.div`
  font-size: 14px;
`


const CountdownCol = ({ value, label }) =>
  <Col>
    <ColValue>{value}</ColValue>
    <ColLabel>{label}</ColLabel>
  </Col>

type Props = {
  endDate: string
}

type State = {
  countdown: {
    days: string,
    hours: string,
    minutes: string,
    seconds: string
  }
}

class Countdown extends React.Component<Props, State> {
  endDate: Object
  interval: Function
  state = {
    countdown: {
      days: '-',
      hours: '-',
      minutes: '-',
      seconds: '-'
    }
  }

  componentWillMount () {
    this.endDate = moment(this.props.endDate)
    this.interval = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount () {
    clearInterval(this.interval)
  }

  tick = () => {
    const now = moment()
    const diff = this.endDate.diff(now, 'seconds')
    const duration = moment.duration(diff, 'seconds')

    this.setState(() => {
      if (diff < 0) {
        clearInterval(this.interval)
        return null
      }
      return {
        countdown: {
          days: parseInt(duration.asDays(), 10),
          hours: duration.hours(),
          minutes: duration.minutes(),
          seconds: duration.seconds()
        }
      }
    })
  }

  render () {
    const { days, hours, minutes, seconds } = this.state.countdown
    return (
      <CountdownStyled>
        <CountdownCol value={days} label='дней' />
        <CountdownCol value={hours} label='часов' />
        <CountdownCol value={minutes} label='минут' />
        <CountdownCol value={seconds} label='секунд' />
      </CountdownStyled>
    )
  }
}


export default Countdown
