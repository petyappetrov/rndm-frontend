// @flow

import * as React from 'react'
import styled from 'styled-components'
import { Flex, Grid } from 'grid-styled'
import { ErrorText } from './'

const CheckboxLabel = styled.label`
  color: #333;
  ${(p) => p.error && 'color: #CD0F21;'}
`

type Props = {
  field: Object,
  form: Object,
  label: string,
}

export default ({
  field: {
    name,
    ...field
  },
  form: {
    touched,
    errors,
    values
  },
  label,
  ...props
}: Props) =>
  <Flex justify='flex-start' align='center'>
    <Grid mb='2px'>
      <input
        type='checkbox'
        name={name}
        id={name}
        checked={values[name]}
        {...field}
        {...props}
      />
    </Grid>
    <Grid ml={5}>
      <CheckboxLabel
        htmlFor={name}
        error={touched[name] && errors[name]}
      >
        {label}
      </CheckboxLabel>
    </Grid>
    {touched[name] && errors[name] &&
      <ErrorText>{errors[name]}</ErrorText>}
  </Flex>
