// @flow

import * as React from 'react'
import DayPicker, { DateUtils } from 'react-day-picker'
import { withHandlers, compose, withProps } from 'recompose'
import { forEach } from 'lodash'
import { Grid, Box } from 'grid-styled'
// import LocaleUtils from 'react-day-picker/moment'
import { Row, Column } from 'components'
import Field, { ErrorText, Label } from './'

const MONTHS = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь']
const WEEKDAYS_LONG = ['Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье']
const WEEKDAYS_SHORT = ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс']


type Props = {
  errors: {
    from: ?string,
    to: ?string
  },
  values: {
    from: ?Date,
    to: ?Date,
  },
  handleDayClick: Function,
  label?: string,
  field: Object,
  form: Object,
  time: boolean
}

const DateRangePicker = ({
  errors,
  values,
  handleDayClick,
  field: {
    name
  },
  form: {
    touched
  },
  label,
  time
}: Props) =>
  <Row>
    <Column w={1}>
      {label &&
        <Label
          htmlFor={name}
          error={touched[name] && errors[name]}
        >
          {label}
        </Label>
      }
      <Grid>
        <DayPicker
          utc
          fixedWeeks
          numberOfMonths={2}
          months={MONTHS}
          weekdaysLong={WEEKDAYS_LONG}
          weekdaysShort={WEEKDAYS_SHORT}
          locale='ru'
          onDayClick={handleDayClick}
          selectedDays={[
            values.from,
            {
              from: values.from,
              to: values.to
            }
          ]}
        />
        {time &&
          <Box mb={15}>
            <Field.TimePicker
              name='endDate'
            />
          </Box>
        }
        <Row>
          <Column w={1} text='center'>
            {errors.from && <ErrorText>{errors.from}</ErrorText>}
            {errors.to && <ErrorText>{errors.to}</ErrorText>}
          </Column>
        </Row>
      </Grid>
    </Column>
  </Row>

const enhance = compose(
  withProps(({
    form: { values, touched, errors },
    names: { from, to }
  }) => ({
    values: {
      from: values[from] && new Date(values[from]),
      to: values[to] && new Date(values[to])
    },
    errors: {
      from: touched[from] && errors[from],
      to: touched[to] && errors[to]
    }
  })),
  withHandlers({
    handleDayClick: ({ values, form, names }) => (day) => {
      const range = DateUtils.addDayToRange(
        day,
        {
          from: values.from,
          to: values.to
        }
      )

      return forEach(range, (value, key) =>
        form.setFieldValue(names[key], value && new Date(value))
      )
    }
  })
)


export default enhance(DateRangePicker)
