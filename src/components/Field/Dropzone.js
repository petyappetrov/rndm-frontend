// @flow

import * as React from 'react'
import ReactDropzone from 'react-dropzone'
import styled from 'styled-components'
import { partial, get } from 'lodash'
import { ErrorText, Label } from './'

const DropzoneStyled = styled(ReactDropzone)`
  width: 100%;
  border: 2px dashed #ccc;
  border-radius: 4px;
  display: flex;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  text-align: center;
  line-height: 22px;
  height: 120px;
  cursor: pointer;
  color: #7d7d7d;
  ${(p) => p.error && 'border-color: #CD0F21;'}
`

const Preview = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  overflow: hidden;
  > img {
    object-fit: cover;
    width: 100%;
    max-height: 600px;
  }
`

type Props = {
  field: Object,
  form: Object,
  label?: string,
  children?: React.Node
}

const Dropzone = ({
  field: {
    name
  },
  form: {
    touched,
    values,
    errors,
    setFieldValue
  },
  label,
  children,
  ...props
}: Props) =>
  <div>
    {label &&
      <Label
        htmlFor={name}
        error={touched[name] && errors[name]}
      >
        {label}
      </Label>
    }
    {values[name]
      ? (
        <Preview>
          <img src={get(values[name], '0.preview')} alt='preview-upload-file' />
        </Preview>
      )
      : (
        <DropzoneStyled
          onDrop={partial(setFieldValue, name)}
          error={touched[name] && errors[name]}
          {...props}
        >
          {({ isDragActive, isDragReject, acceptedFiles }) => {
            if (isDragActive) {
              return <div>Отпустите клавишу мыши, чтобы добавить файл.</div>
            }
            if (isDragReject) {
              return <div>Файл не был принят</div>
            }
            if (acceptedFiles.length) {
              return <div>Файл принят<br />и ждет отправки на сервер</div>
            }
            return (
              <div>
                Кликните или перетащите файл сюда
              </div>
            )
          }}
        </DropzoneStyled>
      )
    }
    {touched[name] && errors[name] &&
      <ErrorText mt={10}>{errors[name]}</ErrorText>}
  </div>


export default Dropzone
