// @flow

import * as React from 'react'
import ReactSelect from 'react-select'
import styled from 'styled-components'
import { Label, ErrorText } from './'

const SelectStyled = styled(ReactSelect)`
  .Select-control {
    ${(p) => p.error && 'border-color: #CD0F21;'}
  }
`

type Props = {
  field: Object,
  form: Object,
  label?: string,
}

const Select = ({
  field: {
    name
  },
  form: {
    touched,
    errors,
    setFieldValue,
    setFieldTouched,
    values
  },
  label,
  ...props
}: Props) =>
  <div>
    {label &&
      <Label
        htmlFor={name}
        error={touched[name] && errors[name]}
      >
        {label}
      </Label>
    }
    <SelectStyled
      clearable={!!values[name]}
      searchable={false}
      name={name}
      value={values[name]}
      error={touched[name] && errors[name]}
      onChange={(field) => setFieldValue(name, field ? field.value : '')}
      onBlur={() => setFieldTouched(name, true)}
      {...props}
    />
    {touched[name] && errors[name] &&
      <ErrorText>{errors[name]}</ErrorText>}
  </div>


export default Select
