// @flow

import * as React from 'react'
import styled from 'styled-components'

import { Label, ErrorText } from './'

export const TextareaStyled = styled.textarea`
  display: block;
  box-sizing: border-box;
  box-shadow: none;
  border: 1px solid #ccc;
  width: 100%;
  padding-left: 10px;
  padding-right: 10px;
  padding-top: 10px;
  min-height: 100px;
  background-color: #fff;
  border-radius: 4px;
  ${(props) => props.error && 'border-color: #CD0F21;'}
`


type Props = {
  field: Object,
  form: Object,
  label?: string,
}

export default ({
  field: {
    name,
    ...field
  },
  form: {
    touched,
    errors
  },
  label,
  ...props
}: Props) =>
  <div>
    {label &&
      <Label
        htmlFor={name}
        error={touched[name] && errors[name]}
      >
        {label}
      </Label>
    }
    <TextareaStyled
      error={touched[name] && errors[name]}
      name={name}
      id={name}
      {...field}
      {...props}
    />
    {touched[name] && errors[name] &&
      <ErrorText>{errors[name]}</ErrorText>}
  </div>
