// @flow

import * as React from 'react'
import TimePicker from 'rc-time-picker'
import moment from 'moment'
import styled from 'styled-components'

import { Label, ErrorText } from './'

const TimePickerStyled = styled(TimePicker)`
  input {
    height: 36px;
    box-sizing: border-box;
    border: 1px solid #ccc;
    width: 100%;
    padding-left: 10px;
    padding-right: 10px;
    background-color: #fff;
    font-size: 16px;
    border-radius: 4px;
    color: #000;
    ${(p) => p.error && 'border-color: #CD0F21;'}
  }
`


type Props = {
  field: Object,
  form: Object,
  label?: string,
}

export default ({
  field: {
    name,
    value
  },
  form: {
    touched,
    errors,
    setFieldValue
  },
  label
}: Props) =>
  <div>
    {label &&
      <Label
        htmlFor={name}
        error={touched[name] && errors[name]}
      >
        {label}
      </Label>
    }
    <div>
      <TimePickerStyled
        name={name}
        id={name}
        placeholder='12:00'
        value={moment(value || new Date())}
        showSecond={false}
        onChange={value => setFieldValue(name, new Date(value.valueOf()))}
      />
    </div>
    {touched[name] && errors[name] &&
      <ErrorText>{errors[name]}</ErrorText>}
  </div>
