// @flow

import * as React from 'react'
import { Field } from 'formik'
import styled from 'styled-components'
import { Grid } from 'grid-styled'
import { partial } from 'lodash'
import { Input } from 'components'
import Checkbox from './Checkbox'
import Dropzone from './Dropzone'
import Textarea from './Textarea'
import Select from './Select'
import DateRangePicker from './DateRangePicker'
import TimePicker from './TimePicker'


export const Label = styled.label`
  display: block;
  color: #333;
  padding-bottom: 5px;
  ${(p) => p.error && 'color: #CD0F21;'}
`

export const ErrorText = styled(Grid)`
  color: #CD0F21;
`

Field.Input = ({ name, ...props }) =>
  <Field
    name={name}
    render={({
      field: {
        onChange,
        ...field
      },
      form: {
        touched,
        errors,
        setFieldValue
      }
    }) =>
      <Input
        name={name}
        error={touched[name] && errors[name]}
        onChange={partial(setFieldValue, name)}
        {...field}
        {...props}
      />
    }
  />


Field.Checkbox = (props) =>
  <Field
    component={Checkbox}
    {...props}
  />


Field.Textarea = (props) =>
  <Field
    component={Textarea}
    {...props}
  />


Field.Select = (props) =>
  <Field
    component={Select}
    {...props}
  />


Field.Dropzone = (props) =>
  <Field
    component={Dropzone}
    {...props}
  />


Field.DateRangePicker = (props) =>
  <Field
    component={DateRangePicker}
    name={props.names.from}
    {...props}
  />


Field.TimePicker = (props) =>
  <Field
    component={TimePicker}
    {...props}
  />


export default Field
