import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import { Row, Column, Container, Text } from 'components'
import { shouldUpdate } from 'recompose'

const Footer = styled((props) => <Column is='footer' {...props} />)`
  width: 100%;
  font-size: 14px;
  background: #414B5B;
  color: #fff;
  a {
    color: #fff;
    font-weight: 700;
    &:hover {
      text-decoration: underline;
    }
  }
`

export default shouldUpdate(() => false)(() =>
  <Footer py={60} mt={[15, 30, 80]}>
    <Container>
      <Text align={['center', 'left']}>
        <Row>
          <Column w={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            <Link to='/rules'>Правила и условия публикации</Link><br />
            <Link to='/faq'>Часто задаваемые вопросы</Link>
          </Column>
          <Column w={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            <Link to='/terms'>Условия использования</Link><br />
            <Link to='/privacy'>Политика конфиденциальности</Link>
          </Column>
          <Column w={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            По просьбам и предложениям: <br />
            <a href='mailto:petyappetrov@yandex.ru'>petyappetrov@yandex.ru</a>
          </Column>
          <Column w={[1, 1/2, 1/2, 1/4]} py={[3, 2, 2, 0]}>
            Разработка сайта –
            {' '}
            <a
              href='https://petyappetrov.github.io'
              target='_blank'
              rel='noopener noreferrer'
            >
              Петр Петров
            </a><br />
            {new Date().getFullYear()} &copy; Все права защищены
          </Column>
        </Row>
      </Text>
    </Container>
  </Footer>
)
