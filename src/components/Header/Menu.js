// @flow

import * as React from 'react'
import { NavLink } from 'react-router-dom'
import styled, { css } from 'styled-components'
import onClickOutside from 'react-onclickoutside'
import { compose, pure } from 'recompose'
import { Flex } from 'grid-styled'
import { Button } from 'components'
import { withWindowWidth } from 'utils'
import Hamburger from './Hamburger'

const DESKTOP_WIDTH = 1024

const MenuItem = styled(NavLink)`
  margin-right: 40px;
  cursor: pointer;
  color: #fff;
  &:hover {
    text-decoration: none;
  }
`

const MobileCSS = css`
  position: absolute;
  flex-direction: column;
  text-align: center;
  top: 0;
  left: 0;
  width: 100%;
  background: #414B5B;
  padding: 100px 0 40px;
  box-sizing: border-box;
  justify-content: center;
  opacity: 0;
  visibility: hidden;
  transition: opacity 0.30s ease,
              visibility 0s linear 0.30s,
              transform 0.30s ease;
  transform: translate3d(0, -30px, 0);
  pointer-events: none;
  ${MenuItem}, a {
    margin: 15px 0;
  }

  ${p => p.show && `
    opacity: 1;
    transform: translate3d(0, 0, 0);
    transition-delay: 0s;
    visibility: visible;
    pointer-events: auto;
  `}
`

const MenuContent = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  ${p => !p.isDesktop && MobileCSS}
`


type Props = {
  width: number,
  auth: Object,
  location: Object
}

class Menu extends React.Component<Props, { show: boolean }> {
  state = {
    show: false
  }

  componentDidUpdate (prevProps: Object) {
    if (
      this.state.show &&
      this.props.location !== prevProps.location
    ) {
      this.onHideMenu()
    }
  }

  onToggleMenu = () => {
    this.setState({ show: !this.state.show })
  }

  onHideMenu = () => {
    this.setState({ show: false })
  }

  handleClickOutside = () => {
    if (this.state.show) {
      this.onHideMenu()
    }
  }

  render () {
    const { width, auth } = this.props
    const isDesktop = width > DESKTOP_WIDTH
    return (
      <Flex align='center'>
        {!isDesktop &&
          <Hamburger
            onClick={this.onToggleMenu}
            active={this.state.show}
          />
        }
        <MenuContent show={this.state.show} isDesktop={isDesktop}>
          <MenuItem to='/'>Главная страница</MenuItem>
          <MenuItem to='/contests'>Найти конкурс</MenuItem>
          {auth.isAuthenticated
            ? (
              <React.Fragment>
                <MenuItem to={`/users/${auth.id}`}>Мой профиль</MenuItem>
                <Button view='other' to='/create'>Создать конкурс</Button>
              </React.Fragment>
            )
            : <Button view='other' to='/signin'>Войти</Button>
          }
        </MenuContent>
      </Flex>
    )
  }
}

export default compose(
  pure,
  withWindowWidth,
  onClickOutside
)(Menu)
