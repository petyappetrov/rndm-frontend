// @flow

import * as React from 'react'
import styled from 'styled-components'
import { Link } from 'react-router-dom'
import FontAwesome from 'react-fontawesome'
import { pure } from 'recompose'
import { Row, Column, Container } from 'components'
import Menu from './Menu'


const Logo = styled(Link)`
  font-size: 28px;
  font-weight: bold;
  color: #000;
  color: #fff;
  position: relative;
  z-index: 1;
  display: flex;
  align-items: center;
  .fa {
    font-size: 36px;
    margin-top: -5px;
  }
  &:hover {
    text-decoration: none;
  }
`

const Header = styled.header`
  width: 100%;
  height: 90px;
  display: flex;
  flex-shrink: 0;
  align-items: center;
  z-index: 10;
  position: relative;
  background-color: ${p => p.transparent ? 'transparent' : '#414B5B'};
`


type Props = {
  auth: Object,
  location: Object
}

export default pure(({ auth, location }: Props) =>
  <Header transparent={location.pathname === '/'}>
    <Container>
      <Row align='center'>
        <Column w={[4/5, 2/3, 2/3, 1/4]}>
          <Logo to='/'>
            <FontAwesome name='gift' />
            &nbsp;
            giveaways.su
          </Logo>
        </Column>
        <Column w={[1/5, 1/3, 1/3, 3/4]}>
          <Row justify='flex-end' align='center'>
            <Column>
              <Menu auth={auth} location={location} />
            </Column>
          </Row>
        </Column>
      </Row>
    </Container>
  </Header>
)
