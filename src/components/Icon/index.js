// @flow

import React from 'react'
import paths from './paths'

type Props = {
  icon: string,
  size?: number,
  color?: string
}

export default ({
  icon,
  size = 28,
  color = '#000',
  ...props
}: Props) =>
  <svg
    width={`${size}px`}
    height={`${size}px`}
    viewBox='0 0 25 25'
    {...props}
  >
    <path d={paths[icon]} style={{ fill: color }} />
  </svg>
