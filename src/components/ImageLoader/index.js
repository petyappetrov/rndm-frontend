// @flow

import * as React from 'react'
import styled from 'styled-components'
import { Spinner } from 'components'
import noImage from './noavatar.png'


const urlAPI = `${process.env.API_HOST}:${process.env.API_PORT}/`

const STATUS_LOADING = 'LOADING'
const STATUS_LOADED = 'LOADED'
const STATUS_ERROR = 'ERROR'

function getImageUrl (src) {
  if (!src) {
    return noImage
  }
  return urlAPI + src
}


const ImageWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  width: ${p => p.width || '100%'};
  height: ${p => p.height || '100%'};
`

const Image = styled.img`
  object-fit: cover;
  width: 100%;
  height: 100%;
  ${p => {
    switch (p.status) {
      case STATUS_LOADED:
        return `
          display: block;
        `
      case STATUS_LOADING:
      case STATUS_ERROR:
      default:
        return `
          display: none;
        `
    }
  }}
`


type Props = {
  src?: string,
  alt?: string,
  width: number,
  height: number
}

type State = {
  status: string
}


export default class ImageLoader extends React.Component<Props, State> {
  state = {
    status: STATUS_LOADING
  }

  componentWillMount () {
    this.image = new Image()
    this.image.src = this.props.src
  }

  onLoad = () => {
    this.setState({
      status: STATUS_LOADED
    })
  }

  onError = () => {
    this.setState({
      status: STATUS_ERROR
    })
  }

  render () {
    const { src, width, height, alt } = this.props
    return (
      <ImageWrapper width={width} height={height}>
        <Image
          src={getImageUrl(src)}
          alt={alt}
          onLoad={this.onLoad}
          onError={this.onError}
          status={this.state.status}
        />
        {this.state.status === STATUS_LOADING &&
          <Spinner />
        }
      </ImageWrapper>
    )
  }
}
