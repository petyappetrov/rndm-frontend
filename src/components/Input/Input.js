// @flow

import * as React from 'react'
import styled from 'styled-components'
import { isFunction } from 'lodash'
import { Label, Text } from 'components'

export const InputStyled = styled.input`
  height: 36px;
  box-sizing: border-box;
  border: 1px solid #ccc;
  width: 100%;
  padding-left: 10px;
  padding-right: 10px;
  background-color: #fff;
  border-radius: 4px;
  ${(p) => p.error && 'border-color: #CD0F21;'}
`

type Props = {
  onChange: () => string,
  onBlur?: Function,
  value?: string,
  error?: string,
  type?: string,
  name: string,
  transform?: Function,
  label?: string
}

const Input = ({
  onChange,
  error,
  transform,
  label,
  name,
  type = 'text',
  ...props
}: Props) =>
  <React.Fragment>
    {label &&
      <Label
        htmlFor={name}
        error={error}
      >
        {label}
      </Label>
    }
    <InputStyled
      error={error}
      name={name}
      type={type}
      id={name}
      onChange={({ target }) =>
        onChange(isFunction(transform) ? transform(target.value) : target.value)
      }
      {...props}
    />
    {error &&
      <Text error>{error}</Text>}
  </React.Fragment>

export default Input
