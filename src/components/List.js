import styled from 'styled-components'

const List = styled.ul`
  padding: 0;
  margin: 0;
  list-style-type: ${p => p.type || 'none'};
  list-style-position: inside;
`

List.Item = styled.li`
  line-height: 1.618em;
`


export default List
