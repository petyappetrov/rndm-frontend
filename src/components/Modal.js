// @flow

import * as React from 'react'
import PropTypes from 'prop-types'
import { Portal } from 'react-portal'
import styled from 'styled-components'
import onClickOutside from 'react-onclickoutside'
import { compose, withHandlers } from 'recompose'
import { isFunction } from 'lodash'
import disableScroll from 'no-scroll'

const ESCAPE_CODE = 27

const sizes = {
  big: '800px',
  normal: '600px',
  small: '300px'
}

const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0,0,0,.35);
  display: flex;
  justify-content: center;
  align-items: flex-start;
  z-index: 15;
`

const ModalHeader = styled.div`
  display: flex;
  padding: 20px;
  font-size: 22px;
  background: #F2F2F2;
  justify-content: space-between;
  align-items: center;
`

const ModalContent = styled.div`
  padding: 20px;
  overflow-y: scroll;
  max-height: 500px;
`

const ModalFooter = styled.div`
  padding: 20px;
  display: flex;
  justify-content: flex-end;
  flex-wrap: wrap;
  background: #F2F2F2;
`

const CloseButton = styled.button`
  background: none;
  border: none;
  cursor: pointer;
  border-radius: 10px;
  padding: 0;
  margin: 0;
  width: 20px;
  height: 20px;
  display: block;
  line-height: 20px;
  font-size: 32px;
  opacity: 0.5;
  font-weight: 300;
  &:hover {
    opacity: 1;
  }
`

const ModalBlock = compose(
  withHandlers({
    handleClickOutside: (props) => props.onRequestClose
  }),
  onClickOutside
)(styled.div`
  background: #fff;
  border-radius: 4px;
  overflow: hidden;
  margin-top: 150px;
  width: ${p => sizes[p.size]};
  box-shadow: 0px 2px 5px rgba(0,0,0,.2);
`)


type Props = {
  children: React.Node,
  size: 'big' | 'normal' | 'small',
  onRequestClose: Function
}

class Modal extends React.Component<*, Props> {
  getChildContext () {
    return {
      onRequestClose: this.props.onRequestClose
    }
  }

  componentDidMount () {
    document.addEventListener('keydown', this.handleKeydown)
    disableScroll.on()
  }

  componentWillUnmount () {
    document.removeEventListener('keydown', this.handleKeydown)
    disableScroll.off()
  }

  handleKeydown = (e) => {
    if (e.keyCode === ESCAPE_CODE) {
      this.props.onRequestClose()
    }
  }

  handleClickOutside = () => {
    this.props.onRequestClose()
  }

  render () {
    const { children, size = 'normal', onRequestClose, ...props } = this.props
    return (
      <Portal
        node={document.getElementById('root')}
        {...props}
      >
        <ModalOverlay>
          <ModalBlock size={size} onRequestClose={onRequestClose}>
            {children}
          </ModalBlock>
        </ModalOverlay>
      </Portal>
    )
  }
}

Modal.childContextTypes = {
  onRequestClose: PropTypes.func
}


Modal.Footer = ModalFooter
Modal.Content = ModalContent


Modal.Header = ({ children }, { onRequestClose }) =>
  <ModalHeader>
    {children}
    {isFunction(onRequestClose) &&
      <CloseButton onClick={onRequestClose} type='button'>&times;</CloseButton>
    }
  </ModalHeader>

Modal.Header.contextTypes = {
  onRequestClose: PropTypes.func
}

export default Modal
