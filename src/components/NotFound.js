import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'
import { Section, Container, Row, Column } from 'components'

const NotFoundTitle = styled.div`
  font-size: 42px;
  font-weight: 700;
  margin: 60px 0 40px;
`

const NotFoundTitleSubtitle = styled(NotFoundTitle)`
  font-size: 28px;
  margin: 30px 0;
`

const NotFound = () =>
  <Container>
    <Row justify='center'>
      <Column w={1/2} text='center' mt={30}>
        <Section>
          <NotFoundTitle>Упс, 404 ошибка</NotFoundTitle>
          <NotFoundTitleSubtitle>Такой странцы не существует</NotFoundTitleSubtitle>
          <Link to='/'>Вернуться на главную</Link>
        </Section>
      </Column>
    </Row>
  </Container>

export default NotFound
