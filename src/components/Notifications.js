// @flow

import * as React from 'react'
import styled from 'styled-components'
import { map } from 'lodash'
import { TransitionMotion, spring } from 'react-motion'

const NotificationsWrapper = styled.div`
  position: fixed;
  top: 15px;
  right: auto;
  left: 50%;
  transform: translateX(-50%);
  margin: auto;
  width: 100%;
  max-width: 400px;
  z-index: 11;
`

const NotificationStyled = styled.div`
  display: flex;
  flex-direction: column;
  border: 1px solid rgba(0,0,0,.4);
  margin-bottom: 15px;
  padding: 10px 15px 12px;
  border-radius: 4px;
  background: #000;
  color: #fff;
  cursor: pointer;
  text-align: center;
  font-weight: 700;
`

const NotificationTitle = styled.div`
  font-size: 16px;
  line-height: 22px;
`

const NotificationMessage = styled.div`
  font-size: 14px;
  margin-top: 5px;
`

type TypesNotification = {
  title: string,
  message: string,
  onClick: Function,
  style: Object
}

type TypesNotifications = {
  items: Array<Object>,
  hideNotification: Function
}

const Notification = ({
  title,
  message,
  onClick,
  style
}: TypesNotification) =>
  <NotificationStyled onClick={onClick} style={style}>
    <NotificationTitle>{title}</NotificationTitle>
    {message &&
      <NotificationMessage>{message}</NotificationMessage>
    }
  </NotificationStyled>

function willLeave () {
  return {
    y: spring(-200, { stiffness: 170, damping: 17 })
  }
}

function willEnter () {
  return {
    y: -200
  }
}

const Notifications = ({
  items,
  hideNotification
}: TypesNotifications) =>
  <NotificationsWrapper>
    <TransitionMotion
      styles={map(items, item => ({
        key: item.id.toString(),
        style: {
          y: spring(0, { stiffness: 170, damping: 17 })
        },
        data: item
      }))}
      willLeave={willLeave}
      willEnter={willEnter}
    >
      {interpolatedStyles =>
        <div>
          {map(interpolatedStyles, ({ data: notification, style }) =>
            <Notification
              key={notification.id}
              style={{
                transform: `translate3d(0, ${style.y}px, 0)`
              }}
              title={notification.title}
              message={notification.message}
              onClick={() => hideNotification(notification.id)}
            />
          )}
        </div>
      }
    </TransitionMotion>
  </NotificationsWrapper>

export default Notifications
