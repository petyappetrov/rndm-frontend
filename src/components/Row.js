import * as React from 'react'
import { Flex } from 'grid-styled'

const Row = (props) =>
  <Flex
    wrap
    mx='-7.5px'
    {...props}
  />

export default Row
