import styled from 'styled-components'
import { space } from 'styled-system'

const Section = styled.div.attrs({ p: p => p.p || [15, 20] })`
  ${space}
  background: #fff;
  border-radius: 4px;
  box-sizing: border-box;
  overflow: ${p => p.overflow || 'visible'};
  box-shadow: ${p => p.theme.shadow};
  ${p => p.fullHeight && 'height: 100%;'}
`


Section.Title = styled.div.attrs({ mb: p => p.mb || 15 })`
  ${space}
  font-weight: 700;
  font-size: 24px;
`

export default Section
