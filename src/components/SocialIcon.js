import React from 'react'
import styled from 'styled-components'
import FontAwesome from 'react-fontawesome'


const Twitter = styled.div`
  background-color: rgb(0, 177, 216);
  border-radius: 50%;
  justify-content: center;
  align-items: center;
  width: ${p => p.size || 100}px;
  height: ${p => p.size || 100}px;
  display: flex;
  flex-shrink: 0;
  color: #fff;
  &:hover {
    text-decoration: none;
  }
`


const Instagram = styled(Twitter)`
  background-color: rgb(226, 50, 117);
`


export default ({ name, bgSize = 50, size = '1x' }) => {
  switch (name) {
    case 'twitter':
      return (
        <Twitter size={bgSize}>
          <FontAwesome size={size} name='twitter' />
        </Twitter>
      )
    case 'instagram':
      return (
        <Instagram size={bgSize}>
          <FontAwesome size={size} name='instagram' />
        </Instagram>
      )
    default:
      return null
  }
}
