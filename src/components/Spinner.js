import styled, { keyframes } from 'styled-components'
import { space } from 'styled-system'

const pulse = keyframes`
  50% {
    background: #000;
  }
`

const Spinner = styled.div.attrs({ m: 15 })`
  ${space};
  position: relative;
  width: 6px;
  height: 24px;
  background: rgba(0, 0, 0, 0.2);
  animation: ${pulse} 750ms infinite;
  animation-delay: 250ms;
  &::before,
  &::after {
    content: '';
    position: absolute;
    display: block;
    height: 16px;
    width: 6px;
    background: rgba(0, 0, 0, 0.2);
    top: 50%;
    transform: translateY(-50%);
    animation: ${pulse} 750ms infinite;
  }
  &::before {
    left: -12px;
  }
  &::after {
    left: 12px;
    animation-delay: 500ms;
  }
`

export default Spinner
