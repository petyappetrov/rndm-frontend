// @flow

import * as React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withStateHandlers, getContext, compose, withProps } from 'recompose'
import { map, partial, forEach, every } from 'lodash'
import { withWindowWidth } from 'utils'
import { Grid, Flex } from 'grid-styled'
import { Button, Section, Row, Column, Text } from 'components'

const StepNavigationList = styled.ul`
  list-style-type: none;
  display: flex;
  padding: 0;
  margin: 0;
  border-bottom: 2px dashed rgba(0,0,0,.1);
`

const StepNavigationPoint = styled.li`
  opacity: .6;
  cursor: pointer;
  position: relative;
  padding-bottom: 20px;
  width: ${p => 100 / p.stepsLength}%;
  text-align: center;
  &::before {
    content: '';
    position: absolute;
    top: auto;
    bottom: -6px;
    left: 0;
    right: 0;
    margin: auto;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    border: 2px solid #000;
    background: #fff;
  }
  ${(p) => p.active && !p.isCompleted && `
    pointer-events: none;
    opacity: 1;
    font-weight: 700;
    &::before {
      background: #000;
    }
  `}
  ${(p) => p.isCompleted && `
    opacity: 1;
    &::before {
      background: #43db84;
      border-color: #43db84;
    }
  `}
  ${(p) => p.active && p.isCompleted && `
    font-weight: 700;
  `}
`

const StepNavigation = ({ steps, counter, goToStep, stepsCompleted }) =>
  <StepNavigationList>
    {map(steps, (step, i) =>
      <StepNavigationPoint
        key={i}
        stepsLength={steps.length}
        isCompleted={stepsCompleted[i]}
        active={counter === i}
        onClick={partial(goToStep, i)}
      >
        <div>
          {step.label}
        </div>
      </StepNavigationPoint>
    )}
  </StepNavigationList>


type Props = {
  steps: Array<{
    label: string,
    component: React.Node
  }>,
  goToPrevStep: Function,
  goToNextStep: Function,
  goToStep: Function,
  counter: number,
  width: number,
  stepsCompleted: Object,
  formik: Object
}


const Steps = ({
  steps,
  counter,
  goToPrevStep,
  goToNextStep,
  goToStep,
  stepsCompleted,
  formik,
  width
}: Props) =>
  <Flex direction='column'>
    <Row>
      <Column w={1}>
        {steps[counter].component}
      </Column>
    </Row>
    <Row mt={15} order={[3, 3, 2]}>
      <Column w={1}>
        <Section>
          {width > 768
            ? (
              <StepNavigation
                steps={steps}
                stepsCompleted={stepsCompleted}
                counter={counter}
                goToStep={goToStep}
              />
            )
            : (
              <Text align='center'>
                {counter + 1} из {steps.length}
              </Text>
            )
          }
        </Section>
      </Column>
    </Row>
    <Row mt={15} order={[2, 2, 3]}>
      <Column w={1}>
        <Section>
          <Row>
            <Column w={1/4}>
              {counter !== 0 &&
                <Grid>
                  <Button onClick={goToPrevStep} view='ghost'>Вернуться</Button>
                </Grid>
              }
            </Column>
            <Column w={3/4}>
              <Text align='right'>
                {counter < steps.length - 1 &&
                  <Grid ml={15}>
                    <Button onClick={goToNextStep} view='ghost'>Следующий шаг</Button>
                  </Grid>
                }
                {formik.isValid && counter === steps.length - 1 &&
                  <Grid ml={15}>
                    <Button type='submit'>Опубликовать</Button>
                  </Grid>
                }
              </Text>
            </Column>
          </Row>
        </Section>
      </Column>
    </Row>
  </Flex>


const getCompleteStateOfStep = (required, { values, errors }) =>
  every(required, field => values[field] && !errors[field])


const enhance = compose(
  withWindowWidth,
  getContext({
    formik: PropTypes.object
  }),
  withProps(({ steps, formik }) => {
    const stepsCompleted = {}
    forEach(steps, (step, i) => {
      stepsCompleted[i] = getCompleteStateOfStep(step.required, formik)
    })
    return {
      stepsCompleted
    }
  }),
  withStateHandlers(
    {
      counter: 0
    },
    {
      goToNextStep: ({ counter }, { steps, formik, stepsCompleted }) => () => {
        if (!stepsCompleted[counter]) {
          return forEach(steps[counter].required, field => formik.setFieldTouched(field, true))
        }
        if (steps.length - 1 > counter) {
          return {
            counter: counter + 1
          }
        }
        return {
          counter
        }
      },
      goToPrevStep: ({ counter }) => () => {
        if (counter > 0) {
          return {
            counter: counter - 1
          }
        }
        return {
          counter
        }
      },
      goToStep: (state, { stepsCompleted }) => (counter) => {
        if (
          stepsCompleted[counter] ||
          (counter !== 0 && stepsCompleted[counter - 1])
        ) {
          return {
            counter
          }
        }
        return null
      }
    }
  )
)

export default enhance(Steps)
