// @ flow

import * as React from 'react'
import styled from 'styled-components'
import { map, isFunction } from 'lodash'
import { Box, Flex } from 'grid-styled'
import { withWindowWidth } from 'utils'

const TableOverlow = styled.div`
  overflow: scroll;
  width: 100%;
`

const Table = styled.table`
  width: 100%;
`

const TableHeader = styled.thead`
  text-align: left;
`

const TableHeaderColumn = styled.th`
  padding-bottom: 0px;
  vertial-align: bottom;
`

const TableBodyColumn = styled.td`
  padding: 15px 10px 0 0;
  &:last-child {
    padding-right: 0;
  }
`


const DesktopTable = ({ data, columns }) =>
  <Table>
    <TableHeader>
      <tr>
        {map(columns, (column, i) =>
          <TableHeaderColumn key={i}>{column.header}</TableHeaderColumn>
        )}
      </tr>
    </TableHeader>
    <tbody>
      {map(data, (contest, i) =>
        <tr key={i}>
          {map(columns, (column, i) =>
            <TableBodyColumn key={i}>
              {isFunction(column.transform)
                ? column.transform(contest[column.accessor], contest)
                : contest[column.accessor]
              }
            </TableBodyColumn>
          )}
        </tr>
      )}
    </tbody>
  </Table>


const MobileTable = ({ data, columns }) =>
  <Flex direction='column'>
    {map(data, (contest, i) =>
      <Box key={i} my={15}>
        {map(columns, (column, i) =>
          <Flex key={i} mb={5} justify='space-between'>
            <Box><strong>{column.header}:</strong></Box>
            <Box>
              {isFunction(column.transform)
                ? column.transform(contest[column.accessor], contest)
                : contest[column.accessor]
              }
            </Box>
          </Flex>
        )}
      </Box>
    )}
  </Flex>


type Props = {
  data: Object,
  width: number,
  columns: {
    header: string,
    accessor: string,
    transform?: Function
  }
}

export default withWindowWidth(({ width, ...props }: Props) =>
  <TableOverlow>
    {width > 768
      ? <DesktopTable {...props} />
      : <MobileTable {...props} />
    }
  </TableOverlow>
)
