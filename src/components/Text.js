import { textAlign, fontSize, fontWeight, space } from 'styled-system'
import styled from 'styled-components'

export default styled.div`
  ${space}
  ${textAlign}
  ${fontSize}
  ${fontWeight}
  line-height: 1.618em;
  ${p => p.error && 'color: #CD0F21'}
`
