import styled from 'styled-components'
import { fontSize, space } from 'styled-system'


export default styled.div.attrs({
  mt: p => p.mt || [35, 65],
  mb: p => p.mb || [20, 30],
})`
  ${fontSize}
  ${space}
  font-weight: 700;
  word-wrap: break-word;
  font-size: 36px;
  line-height: 58.248px;
  @media (max-width: 768px) {
    font-size: 32px;
    line-height: 37.376px
  }
  @media (max-width: 480px) {
    font-size: 28px;
    line-height: 32.5304px
  }
`
