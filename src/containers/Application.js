// @flow

import * as React from 'react'
import { map, get } from 'lodash'
import { Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { Main, SignIn, SignUp, CreateContest, Contests, User, Statics } from 'containers'
import { Notifications, Header, Footer, NotFound } from 'components'
import { hideNotification as hideNotificationAction } from 'redux/actions/notifications'


const routes = [
  {
    path: '/',
    component: Main,
    exact: true
  },
  {
    path: '/signin',
    component: SignIn
  },
  {
    path: '/signup',
    component: SignUp
  },
  {
    path: '/contests',
    component: Contests
  },
  {
    path: '/create',
    component: CreateContest
  },
  {
    path: '/users/:id',
    component: User
  },
  {
    path: '/privacy',
    component: Statics
  },
  {
    path: '/terms',
    component: Statics
  },
  {
    path: '/faq',
    component: Statics
  },
  {
    path: '/rules',
    component: Statics
  },
  {
    path: '*',
    component: NotFound
  }
]

const Content = styled.main`
  flex: 1 0 auto;
`

type Props = {
  location: Object,
  auth: Object,
  notifications: Array<Object>,
  hideNotification: Function
}


class Application extends React.Component<Props> {
  componentDidUpdate (prevProps: Object) {
    const { location } = this.props
    const jumpToTop = get(location.state, 'jumpToTop', true)
    if (location !== prevProps.location && jumpToTop) {
      window.scrollTo(0, 0)
    }
  }
  render () {
    const {
      notifications,
      hideNotification,
      auth,
      location
    } = this.props
    return (
      <React.Fragment>
        <Header
          auth={auth}
          location={location}
        />
        <Content>
          <Switch>
            {map(routes, (route, i) =>
              <Route
                key={i}
                path={route.path}
                component={route.component}
                exact={route.exact}
              />
            )}
          </Switch>
          <Notifications
            items={notifications}
            hideNotification={hideNotification}
          />
        </Content>
        <Footer />
      </React.Fragment>
    )
  }
}


const mapStateToProps = ({ notifications, auth }) => ({
  auth,
  notifications
})


const mapDispatchToProps = (dispatch: Function) => ({
  hideNotification: (id: string) => dispatch(hideNotificationAction(id)),
})


export default connect(mapStateToProps, mapDispatchToProps)(Application)
