// @flow

import * as React from 'react'
import { Formik, Form } from 'formik'
import { Field, Row, Column, Button, Modal } from 'components'


type Props = {
  contest: Object,
  updateContest: Function,
  onCloseModal: Function,
  removeContest: Function,
}

const ContestEdit = ({ contest, updateContest, onCloseModal, removeContest }: Props) =>
  <Modal
    isOpened
    size='normal'
    contentLabel='constest-edit'
    onRequestClose={onCloseModal}
  >
    <Formik
      initialValues={contest}
      onSubmit={updateContest}
      // validationSchema={validationSchema}
    >
      <Form>
        <Modal.Header>
          <Row>
            <Column>
              <strong>Редактировать конкурс</strong>
            </Column>
          </Row>
        </Modal.Header>
        <Modal.Content>
          <Row>
            <Column w={1}>
              <Field.Input
                label='Название'
                name='title'
                placeholder='Введите текст'
              />
            </Column>
          </Row>
          <Row mt={15}>
            <Column w={1}>
              <Field.Textarea
                label='Описание'
                name='description'
                placeholder='Введите текст'
              />
            </Column>
          </Row>
          <Row mt={15}>
            <Column w={1}>
              <Field.Dropzone
                name='image'
                accept='image/jpeg, image/jpg, image/png'
                label='Загрузить фото конкурса'
              />
            </Column>
          </Row>
        </Modal.Content>
        <Modal.Footer>
          <Button
            w={[1, 'auto']}
            order={[2, 1]}
            mr='auto'
            mt={[15, 0]}
            onClick={removeContest}
            view='danger'
          >
            Удалить конкурс
          </Button>
          <Button
            w={[1, 'auto']}
            order={[3, 2]}
            mt={[15, 0]}
            mr={[0, 15]}
            view='ghost'
            onClick={onCloseModal}
          >
            Отменить
          </Button>
          <Button
            w={[1, 'auto']}
            order={[1, 3]}
            type='submit'
          >
            Редактировать
          </Button>
        </Modal.Footer>
      </Form>
    </Formik>
  </Modal>

export default ContestEdit
