// @flow

import * as React from 'react'
import { Row, Column, Button, Modal } from 'components'


type Props = {
  onCloseModal: Function,
}


const ContestEdit = ({ onCloseModal }: Props) =>
  <Modal
    isOpened
    size='big'
    contentLabel='contest-statistics'
    onRequestClose={onCloseModal}
  >
    <Modal.Header>
      <Row>
        <Column>
          <strong>Статистика конкурса</strong>
        </Column>
      </Row>
    </Modal.Header>
    <Modal.Content>
      TODO: Create
    </Modal.Content>
    <Modal.Footer>
      <Row>
        <Column w={1} text='right'>
          <Button
            view='ghost'
            onClick={onCloseModal}
          >
            Закрыть
          </Button>
        </Column>
      </Row>
    </Modal.Footer>
  </Modal>

export default ContestEdit
