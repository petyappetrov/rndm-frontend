// @flow

import * as React from 'react'
import { Route, Switch, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { updateContest, removeContest, getContest } from 'redux/actions/contests'
import { selectContest, selectIsOwner } from 'redux/selectors'
import { isEmpty } from 'lodash'
import moment from 'moment'
import { Grid, Flex } from 'grid-styled'
import { sliceString } from 'utils'
import {
  Container,
  Row,
  Column,
  Title,
  Section,
  ImageLoader,
  Countdown,
  Button,
  Text,
  List,
  Spinner
} from 'components'
import ContestEdit from './ContestEdit'
import ContestStatistics from './ContestStatistics'


type Props = {
  match: Object,
  history: Object,
  contest: Object,
  isLoading: boolean,
  isOwner: boolean,
  getContest: Function,
  updateContest: Function,
  removeContest: Function
}


class ContestEntry extends React.Component<Props> {
  componentWillMount () {
    this.props.getContest(this.props.match.params.id)
  }

  onCloseModal = () => {
    this.props.history.push({
      pathname: this.props.match.url,
      state: {
        jumpToTop: false
      }
    })
  }

  onUpdateContest = (values) => {
    this.props.updateContest(values)
  }

  onRemoveContest = () => {
    this.props.removeContest(this.props.match.params.id)
  }

  render () {
    const { match, contest, isLoading, isOwner } = this.props
    if (isEmpty(contest) && isLoading) {
      return <Flex justify='center'><Spinner /></Flex>
    }
    return (
      <React.Fragment>
        <Container>
          <Title>{contest.title}</Title>
          <Row>
            <Column w={[1, 1, 1/2, 2/5]} mb={15}>
              <Section p={[0]} overflow='hidden'>
                <ImageLoader src={contest.imageUrl} />
              </Section>
              {isOwner
                ? (
                  <Section mt={15}>
                    <Section.Title>Управление конкурсом</Section.Title>
                    <Button
                      view='ghost'
                      to={{
                        pathname: `${match.url}/statistics`,
                        state: {
                          jumpToTop: false
                        }
                      }}
                    >
                      Статистика
                    </Button>
                    <Grid ml={15}>
                      <Button
                        view='ghost'
                        to={{
                          pathname: `${match.url}/edit`,
                          state: {
                            jumpToTop: false
                          }
                        }}
                      >
                        Редактировать
                      </Button>
                    </Grid>
                  </Section>
                )
                : (
                  <Section mt={15}>
                    <Section.Title>Организатор конкурса</Section.Title>
                    <Link to={`/users/${contest.createdBy}`}>
                      {contest.author && contest.author.name}
                    </Link>
                    {contest.author && contest.author.about &&
                      <Text pt={10}>
                        {sliceString(contest.author.about, 150)}
                      </Text>
                  }
                  </Section>
                )
              }
              {contest.endDate &&
                <Section mt={15}>
                  <Section.Title>До конца конкурса осталось</Section.Title>
                  <Countdown endDate={contest.endDate} />
                </Section>
              }
            </Column>
            <Column w={[1, 1, 1/2, 3/5]}>
              <Section>
                <Section.Title>Условия конкурса</Section.Title>
                <List>
                  <List.Item>
                    <strong>Кол-во победителей:</strong> {contest.countWinners}
                  </List.Item>
                  <List.Item>
                    <strong>Тип:</strong> {contest.type}
                  </List.Item>
                  <List.Item>
                    <strong>Город:</strong> {contest.city || 'Участвуют все города'}
                  </List.Item>
                  <List.Item>
                    <strong>Начало:</strong> {moment(contest.startDate).format('DD.MM.YYYY')}
                  </List.Item>
                  <List.Item>
                    <strong>Конец:</strong> {moment(contest.endDate).format('DD.MM.YYYY, HH:mm')}
                  </List.Item>
                </List>
              </Section>
              <Section mt={15}>
                <Section.Title>Условия участия</Section.Title>
                <List type='decimal'>
                  <List.Item>Оставить комментарий под фото</List.Item>
                  <List.Item>Отметить в комментариях 2-х друзей</List.Item>
                  <List.Item>Подписаться на наш аккаунт</List.Item>
                </List>
              </Section>
              <Section mt={15}>
                <Section.Title>Описание конкурса</Section.Title>
                <p>{contest.description}</p>
              </Section>
            </Column>
          </Row>
        </Container>
        {isOwner &&
          <Switch>
            <Route
              path={`${match.url}/edit`}
              render={(props: Object) =>
                <ContestEdit
                  {...props}
                  contest={contest}
                  onCloseModal={this.onCloseModal}
                  updateContest={this.onUpdateContest}
                  removeContest={this.onRemoveContest}
                />
              }
            />
            <Route
              path={`${match.url}/statistics`}
              render={(props: Object) =>
                <ContestStatistics
                  {...props}
                  onCloseModal={this.onCloseModal}
                />
              }
            />
          </Switch>
        }
      </React.Fragment>
    )
  }
}


const mapStateToProps = (state, ownProps) => ({
  contest: selectContest(state, ownProps),
  isOwner: selectIsOwner(state, ownProps),
  isLoading: state.contests.isLoading
})


const mapDispatchToProps = {
  getContest,
  updateContest,
  removeContest
}


export default connect(mapStateToProps, mapDispatchToProps)(ContestEntry)
