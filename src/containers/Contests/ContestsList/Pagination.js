import React from 'react'
import styled from 'styled-components'
import { map, range, partial } from 'lodash'
import { Box, Flex } from 'grid-styled'


const Page = styled.a`
  color: #000;
  cursor: pointer;
  user-select: none;
  font-weight: 700;
  ${p => p.active && `
    text-decoration: underline;
    pointer-events: none;
    cursor: default;
  `}
  ${p => p.disabled && `
    pointer-events: none;
    cursor: default;
  `}
`

const Pagination = styled.div`
  display: flex;
  height: 38px;
  border-radius: 20px;
  overflow: hidden;
  background: #fff;
  align-items: center;
  padding: 0 7px;
  box-shadow: ${p => p.theme.shadow};
`

const Arrow = styled(Page)`
  font-size: 14px;
`

export default ({ count, onChangePage, page }) =>
  <Flex ml='auto' align='center' justify='center' mt={[15, 30, 60]}>
    <Pagination>
      <Box px={10}>
        <Arrow
          onClick={partial(onChangePage, page - 1)}
          disabled={page <= 0}
        >
          ❮
        </Arrow>
      </Box>
      {map(range(1, (count / 8) + 1), (num, i) =>
        <Box key={i} px={10}>
          <Page
            onClick={partial(onChangePage, i)}
            active={parseInt(page, 10) === i}
          >
            {num}
          </Page>
        </Box>
      )}
      <Box px={10}>
        <Arrow
          onClick={partial(onChangePage, page + 1)}
          disabled={page >= (count / 8) - 1}
        >
          ❯
        </Arrow>
      </Box>
    </Pagination>
  </Flex>
