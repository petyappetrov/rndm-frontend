// @flow

import * as React from 'react'
import { map, isEmpty, forEach, pick, isEqual, omit } from 'lodash'
import { compose } from 'redux'
import { connect } from 'react-redux'
import StackGrid, { transitions } from 'react-stack-grid'
import Select from 'react-select'
import { Flex, Grid, Box } from 'grid-styled'
import { getContests } from 'redux/actions/contests'
import { selectFilteredContests } from 'redux/selectors'
import {
  ContestItem,
  Title,
  Container,
  Column,
  Row,
  Section,
  Button,
  Spinner,
  Text,
  Input
} from 'components'
import { type, city, socialNetwork, condition, sortFields, sortDirections } from 'utils/options'
import { withQuery, possibleFilters, withWindowWidth } from 'utils'
import Pagination from './Pagination'


const { scaleUp } = transitions

const DESKTOP_WIDTH = 1024
const TABLET_WIDTH = 768
const MOBILE_WIDTH = 480
const ENTER_CODE = 13


function getColumnWidth (width) {
  if (width > DESKTOP_WIDTH) {
    return '25%'
  } else if (width > TABLET_WIDTH && width < DESKTOP_WIDTH) {
    return '33.333%'
  } else if (width > MOBILE_WIDTH && width < TABLET_WIDTH) {
    return '50%'
  }
  return '100%'
}

type Props = {
  getContests: Function,
  updateQuery: Function,
  contests: Array<Object>,
  filters: Object,
  count: number,
  width: number,
  isLoading: boolean
}

type State = {
  search: string,
  filtersIsShown: boolean
}

const humanReadablePage = (page) => parseInt(page, 10) - 1


class Contests extends React.Component<Props, State> {
  state = {
    search: this.props.filters.search || '',
    filtersIsShown: this.props.width > DESKTOP_WIDTH
  }

  componentDidMount () {
    this.loadContest(this.props)
  }

  componentWillReceiveProps (nextProps) {
    if (!isEqual(this.props.filters, nextProps.filters)) {
      this.loadContest(nextProps)
    }
    this.handleResizeWindow(nextProps.width)
  }

  loadContest = ({ filters: { page, ...filters } }) => {
    if (page) {
      this.props.getContests({
        ...filters,
        limit: humanReadablePage(page) * 8 + 8,
        skip: humanReadablePage(page) * 8
      })
    } else {
      this.props.getContests(filters)
    }
  }

  handleFilter = (field) => (filter) => {
    this.props.updateQuery({
      [field]: filter ? filter.value : null,
      page: null
    })
  }

  onChangeSearch = ({ target }) => {
    this.setState({
      search: target.value
    })
  }

  onSearch = () => {
    this.props.updateQuery({
      search: this.state.search !== '' ? this.state.search : null
    })
  }

  onChangePage = (page) => {
    this.props.updateQuery({ page: page === 0 ? null : page + 1 })
  }

  onToggleFilter = (e) => {
    e.preventDefault()
    this.setState({
      filtersIsShown: !this.state.filtersIsShown
    })
  }

  resetFilters = () => {
    const filters = {}
    forEach(this.props.filters, (value, key) =>
      filters[key] = null
    )
    this.props.updateQuery(filters)
  }

  handleResizeWindow = (width) => {
    if (width > DESKTOP_WIDTH && !this.state.filtersIsShown) {
      this.setState({ filtersIsShown: true })
    } else if (width < DESKTOP_WIDTH && this.state.filtersIsShown) {
      this.setState({ filtersIsShown: false })
    }
  }

  handleKeyPressToSearch = (e) => {
    if (e.charCode === ENTER_CODE) {
      this.onSearch()
    }
  }

  render () {
    const { contests, isLoading, filters, count, width } = this.props

    return (
      <Container>
        <Title>
          <Flex align='center'>
            <Grid>Найти конкурс</Grid>
            {isLoading &&
              <Grid ml={30}><Spinner size={20} m={0} /></Grid>
            }
          </Flex>
        </Title>
        <Row mb={15}>
          <Column w={1}>
            <Section py={[15, 20, 40]} px={[15, 20, 40, 120]}>
              <Row>
                <Column w={[1, 1, 3/4]}>
                  <Input
                    placeholder='Введите название'
                    value={this.state.search}
                    onChange={this.onChangeSearch}
                    onKeyPress={this.handleKeyPressToSearch}
                  />
                </Column>
                <Column w={[1, 1, 1/4]} mt={[15, 15, 0]}>
                  <Button
                    w={1}
                    onClick={this.onSearch}
                  >
                    Найти
                  </Button>
                </Column>
              </Row>
            </Section>
            <Section mt={15} px={[15, 20, 40, 120]}>
              {this.state.filtersIsShown &&
                <Box mb={15} mt={15}>
                  <Row>
                    <Column w={[1, 1, 1/4, 1/4]} pb={[15, 15, 0, 0]}>
                      <Select
                        searchable={false}
                        clearable={false}
                        onChange={this.handleFilter('socialNetwork')}
                        value={filters.socialNetwork}
                        placeholder='Все социальные сети'
                        options={socialNetwork}
                      />
                    </Column>
                    <Column w={[1, 1, 1/4, 1/4]} pb={[15, 15, 0, 0]}>
                      <Select
                        searchable={false}
                        clearable={false}
                        onChange={this.handleFilter('city')}
                        value={filters.city}
                        options={city}
                        placeholder='Все города'
                      />
                    </Column>
                    <Column w={[1, 1, 1/4, 1/4]} pb={[15, 15, 0, 0]}>
                      <Select
                        searchable={false}
                        clearable={false}
                        onChange={this.handleFilter('type')}
                        value={filters.type}
                        placeholder='Все типы конкурсов'
                        options={type}
                      />
                    </Column>
                    <Column w={[1, 1, 1/4, 1/4]}>
                      <Select
                        searchable={false}
                        clearable={false}
                        onChange={this.handleFilter('condition')}
                        value={filters.condition}
                        placeholder='Только активные'
                        options={condition}
                      />
                    </Column>
                  </Row>
                  <Row justify='center' mt={15}>
                    <Column w={[1, 1, 1/4, 1/4]} pb={[15, 15, 0, 0]}>
                      <Select
                        searchable={false}
                        clearable={false}
                        onChange={this.handleFilter('sortField')}
                        value={filters.sortField}
                        placeholder='Сортировать'
                        options={sortFields}
                      />
                    </Column>
                    <Column w={[1, 1, 1/4, 1/4]} pb={[15, 15, 0, 0]}>
                      <Select
                        searchable={false}
                        clearable={false}
                        onChange={this.handleFilter('sortDirection')}
                        value={filters.sortDirection}
                        placeholder='Упорядочить'
                        options={sortDirections}
                      />
                    </Column>
                    <Column w={[1, 1, 1/4, 1/4]} pb={[15, 15, 0, 0]}>
                      <Button
                        view='ghost'
                        w={1}
                        onClick={this.resetFilters}
                        disabled={isEmpty(omit(filters, 'page'))}
                      >
                        Сбросить
                      </Button>
                    </Column>
                  </Row>
                </Box>
              }
              {width < DESKTOP_WIDTH &&
                <Row justify='center'>
                  <Column>
                    <a href='#' onClick={this.onToggleFilter}>
                      {this.state.filtersIsShown ? 'Скрыть' : 'Фильтры'}
                    </a>
                  </Column>
                </Row>
              }
            </Section>
          </Column>
        </Row>
        {isEmpty(contests) && !isLoading
          ? (
            <Text align='center' mt={60}>
              По вашему запросу ничего не найдено<br />
              Попробуйте изменить параметры поиска
            </Text>
          )
          : (
            <React.Fragment>
              <StackGrid
                style={{ minHeight: 500 }}
                monitorImagesLoaded
                gutterWidth={15}
                gutterHeight={15}
                appearDelay={100}
                duration={300}
                appear={scaleUp.appear}
                appeared={scaleUp.appeared}
                enter={scaleUp.enter}
                entered={scaleUp.entered}
                leaved={scaleUp.leaved}
                columnWidth={getColumnWidth(width)}
              >
                {map(contests, (contest) =>
                  <ContestItem
                    key={contest._id}
                    contest={contest}
                  />
                )}
              </StackGrid>
              {count / 8 > 1 &&
                <Pagination
                  count={count}
                  page={filters.page ? humanReadablePage(filters.page) : 0}
                  onChangePage={this.onChangePage}
                />
              }
            </React.Fragment>
          )
        }
      </Container>
    )
  }
}


const mapDispatchToProps = {
  getContests
}

const mapStateToProps = (state, ownProps) => ({
  contests: selectFilteredContests(state, ownProps) || [],
  isLoading: state.contests.isLoading,
  count: state.contests.count,
  filters: pick(ownProps.query, possibleFilters)
})

export default compose(
  withQuery,
  withWindowWidth,
  connect(mapStateToProps, mapDispatchToProps)
)(Contests)
