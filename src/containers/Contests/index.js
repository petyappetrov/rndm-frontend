// @flow

import * as React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { Spinner } from 'components'
import { Flex } from 'grid-styled'
import Contest from './Contest'
import ContestsList from './ContestsList'


const ContestsEntry = ({ auth, match }) => {
  if (auth.isLoading) {
    return <Flex justify='center'><Spinner /></Flex>
  }
  return (
    <Switch>
      <Route path={match.url} component={ContestsList} exact />
      <Route path={`${match.url}/:id`} component={Contest} />
      <Redirect to={match.url} />
    </Switch>
  )
}


const mapStateToProps = ({ auth }): Object => ({
  auth
})


export default connect(mapStateToProps)(ContestsEntry)
