// @flow

import * as React from 'react'
import { connect } from 'react-redux'
import { Formik, Form } from 'formik'
import yup from 'yup'
import moment from 'moment'
import { map, range } from 'lodash'
import { Section, Field, Steps, Row, Column } from 'components'
import { createContest as createContestAction } from 'redux/actions/contests'
import { type, city, find } from 'utils/options'


const validationSchema = yup.object({
  title: yup
    .string()
    .min(3)
    .max(60)
    .required('Вы не дали название к конкурсу'),
  image: yup
    .array()
    .required('Добавьте изображение пожалуйста'),
  description: yup
    .string()
    .min(10)
    .max(600)
    .required('Описание обязательно для заполнения'),
  type: yup
    .string()
    .matches(/repost|comment/)
    .required('Выберите тип конкурса'),
  find: yup
    .string()
    .matches(/randomFromAuto|randomFromButton|manual/)
    .required('Выберите каким способом определить победителя'),
  countWinners: yup
    .number()
    .min(1)
    .max(10)
    .required('Выберите количество победителей'),
  city: yup
    .string()
    .required('Выберите город'),
  tag: yup
    .string()
    .required('Укажите уникальный хэштег'),
  startDate: yup
    .date()
    .required('Не выбрана дата начала конкурса'),
  endDate: yup
    .date()
    .required('Не выбрана дата окончания конкурса')
})


type Props = {
  createContest: Function
}

const CreateContest = ({ createContest }: Props) =>
  <Formik
    initialValues={{
      title: '',
      image: '',
      description: '',
      type: '',
      find: '',
      tag: '',
      countWinners: '',
      startDate: null,
      endDate: null,
      socialNetwork: 'instagram'
    }}
    onSubmit={(values) =>
      createContest({
        ...values,
        endDate: moment(values.endDate).format()
      })
    }
    validateOnBlur
    validationSchema={validationSchema}
  >
    <Form>
      <Steps
        steps={[
          {
            label: 'Параметры',
            required: ['title', 'type', 'find', 'countWinners'],
            component: (
              <Row>
                <Column w={[1, 1, 7/12]} order={[2, 2, 1]}>
                  <Section>
                    <Section.Title>Параметры</Section.Title>
                    <Row>
                      <Column w={1}>
                        <Field.Input
                          label='Название'
                          name='title'
                          placeholder='Введите текст'
                        />
                      </Column>
                    </Row>
                    <Row mt={15}>
                      <Column w={[1, 1, 1/2]}>
                        <Field.Select
                          label='Тип конкурса'
                          name='type'
                          options={type}
                          placeholder='Выберите'
                        />
                      </Column>
                      <Column w={[1, 1, 1/2]} mt={[15, 15, 0]}>
                        <Field.Select
                          label='Определение победителя'
                          name='find'
                          options={find}
                          placeholder='Выберите'
                        />
                      </Column>
                    </Row>
                    <Row mt={15}>
                      <Column w={[1, 1, 1/2]}>
                        <Field.Select
                          label='Количество победителей'
                          name='countWinners'
                          labelKey='value'
                          options={map(range(1, 11), (value) => ({ value }))}
                          placeholder='Выберите'
                        />
                      </Column>
                    </Row>
                  </Section>
                </Column>
                <Column w={[1, 1, 5/12]} order={[1, 1, 2]} pb={[15, 15, 0]}>
                  <Section fullHeight>
                    <Section.Title>Подсказка</Section.Title>
                    Это обязательное поле. Название конкурса должно
                    быть больше&nbsp;3&nbsp;и&nbsp;меньше 20&nbsp;символов.
                    Оно будет отображаться в&nbsp;списке всех конкурсов. Придумайте
                    текст, который будет вызывать внимание.
                  </Section>
                </Column>
              </Row>
            )
          },
          {
            label: 'Условия и описание',
            required: ['description', 'city', 'tag'],
            component: (
              <Row>
                <Column w={[1, 1, 7/12]} order={[2, 2, 1]}>
                  <Section>
                    <Section.Title>Условия и описание</Section.Title>
                    <Row>
                      <Column w={[1, 1, 1/2]}>
                        <Field.Select
                          label='Город'
                          name='city'
                          options={city}
                          placeholder='Выберите'
                        />
                      </Column>
                      <Column w={[1, 1, 1/2]} mt={[15, 15, 0]}>
                        <Field.Input
                          label='Уникальный хэштег'
                          name='tag'
                          transform={value => {
                            if (value && !value.match(/^#/)) {
                              return `#${value}`
                            }
                            return value
                          }}
                          placeholder='Введите хэштег'
                        />
                      </Column>
                    </Row>
                    <Row mt={15}>
                      <Column w={1}>
                        <Field.Textarea
                          label='Описание'
                          name='description'
                          placeholder='Введите текст'
                        />
                      </Column>
                    </Row>
                  </Section>
                </Column>
                <Column w={[1, 1, 5/12]} order={[1, 1, 2]} pb={[15, 15, 0]}>
                  <Section fullHeight>
                    <Section.Title>Подсказка</Section.Title>
                    Опишите конкурс до мельчайших подробностей. Текст должен превышать 20
                    символов. Текстовое поле можно увеличить потянув за нижний-правый угол.
                  </Section>
                </Column>
              </Row>
            )
          },
          {
            label: 'Изображение',
            required: ['image'],
            component: (
              <Row>
                <Column w={[1, 1, 7/12]} order={[2, 2, 1]}>
                  <Section>
                    <Section.Title>Изображение</Section.Title>
                    <Field.Dropzone
                      name='image'
                      accept='image/jpeg, image/jpg, image/png'
                      label='Загрузить фото конкурса'
                    />
                  </Section>
                </Column>
                <Column w={[1, 1, 5/12]} order={[1, 1, 2]} pb={[15, 15, 0]}>
                  <Section fullHeight>
                    <Section.Title>Подсказка</Section.Title>
                    Загрузите изображение, которое вы&nbsp;будете выкладывать в&nbsp;свой
                    инстаграм. Размер файла не&nbsp;должно превышать 5&nbsp;мегабайт.
                    Допустимые форматы: jpg, jpeg, png.
                  </Section>
                </Column>
              </Row>
            )
          },
          {
            label: 'Дата и время',
            required: ['startDate', 'endDate'],
            component: (
              <Row>
                <Column w={[1, 1, 7/12]} order={[2, 2, 1]}>
                  <Section>
                    <Section.Title>Дата и время проведения</Section.Title>
                    <Field.DateRangePicker
                      time
                      names={{
                        from: 'startDate',
                        to: 'endDate'
                      }}
                    />
                  </Section>
                </Column>
                <Column w={[1, 1, 5/12]} order={[1, 1, 2]} pb={[15, 15, 0]}>
                  <Section fullHeight>
                    <Section.Title>Подсказка</Section.Title>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Doloremque nobis unde, fuga nihil iste eius possimus, atque
                    veritatis eum aspernatur voluptatum non quas repudiandae veniam
                    reprehenderit fugiat aperiam.
                  </Section>
                </Column>
              </Row>
            )
          }
        ]}
      />
    </Form>
  </Formik>


const mapDispatchToProps = (dispatch) => ({
  createContest: (values) => dispatch(createContestAction(values))
})

export default connect(null, mapDispatchToProps)(CreateContest)
