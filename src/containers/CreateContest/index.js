// @flow

import * as React from 'react'
import { connect } from 'react-redux'
import { Link, Switch, Route, Redirect } from 'react-router-dom'
import { Section, Title, Row, Column, Container, SocialIcon } from 'components'
import { Box, Flex } from 'grid-styled'
import Instagram from './Instagram'


const SelectToSocialNetwork = () =>
  <Row>
    <Column w={[1, 1, 7/12, 7/12]} order={[2, 2, 1, 1]}>
      <Section>
        <Section.Title>Социальная сеть</Section.Title>
        <Flex pt={15} pb={45}>
          <Box>
            <Link to='/create/twitter'>
              <SocialIcon name='twitter' size='3x' bgSize={100} />
            </Link>
          </Box>
          <Box ml={30}>
            <Link to='/create/instagram'>
              <SocialIcon name='instagram' size='3x' bgSize={100} />
            </Link>
          </Box>
        </Flex>
      </Section>
    </Column>
    <Column w={[1, 1, 5/12, 5/12]} order={[1, 1, 2, 2]} pb={[15, 15, 0, 0]}>
      <Section fullHeight>
        <Section.Title>Подсказка</Section.Title>
        Вам надо выбрать социальную сеть, где собираетесь организвать свой конкурс.
        Пока доступны только Instagram и Twitter.
      </Section>
    </Column>
  </Row>


const CreateContest = ({ auth }) => {
  if (!auth.isAuthenticated) {
    return <Redirect to='/signin' />
  }
  return (
    <Container>
      <Title>Создать конкурс</Title>
      <Switch>
        <Route path='/create' exact component={SelectToSocialNetwork} />
        <Route path='/create/instagram' component={Instagram} />
        <Redirect to='/create' />
      </Switch>
    </Container>
  )
}


export default connect(({ auth }): Object => ({ auth }))(CreateContest)
