// @flow

import * as React from 'react'
import styled from 'styled-components'
import { Title, Row, Column, Container, Icon, Text } from 'components'

const FeaturesLayout = styled.div`
  background: #fff;
  display: flex;
  padding: 20px 0 60px;
`

const Feature = styled.div`
  opacity: .8;
  margin: 10px 0 20px;
  padding-right: 15px;
`

const FeatureIcon = styled.div`
  width: 80px;
  height: 80px;
  display: flex;
  svg {
    width: 55px;
  }
`

const FeatureTitle = styled.div`
  font-size: 18px;
  font-weight: 700;
  margin: 10px 0 15px;
`

const Features = () =>
  <FeaturesLayout>
    <Container>
      <Title>Возможности сайта</Title>
      <Row>
        <Column w={[1, 1/2, 1/2, 1/4]}>
          <Feature>
            <FeatureIcon>
              <Icon icon='cup' size={55} />
            </FeatureIcon>
            <FeatureTitle>Определение победителя</FeatureTitle>
            <Text>
              Выберите подходящий вам тип конкурса и
              пусть машина работает вместо вас.
            </Text>
          </Feature>
        </Column>
        <Column w={[1, 1/2, 1/2, 1/4]}>
          <Feature>
            <FeatureIcon>
              <Icon icon='search' size={55} />
            </FeatureIcon>
            <FeatureTitle>Выявление ботов</FeatureTitle>
            <Text>
              Сервис находит потенциальных ботов и предоставляет их вам.
              Вы сами решаете, что с ними делать.
            </Text>
          </Feature>
        </Column>
        <Column w={[1, 1/2, 1/2, 1/4]}>
          <Feature>
            <FeatureIcon>
              <Icon icon='rating' size={55} />
            </FeatureIcon>
            <FeatureTitle>Анализ статистики</FeatureTitle>
            <Text>
              Предоставление статистики от начала конкурса –
              Количество новых подписчиков, охват аудиотории и т. д.
            </Text>
          </Feature>
        </Column>
        <Column w={[1, 1/2, 1/2, 1/4]}>
          <Feature>
            <FeatureIcon>
              <Icon icon='coding' size={55} />
            </FeatureIcon>
            <FeatureTitle>Честность</FeatureTitle>
            <Text>
              Генерация всех случаных чисел осуществляется через проверенный сервис random.org
            </Text>
          </Feature>
        </Column>
      </Row>
    </Container>
  </FeaturesLayout>

export default Features
