import * as React from 'react'
import styled from 'styled-components'
import Disco from 'rc-disco'
import { Button, Title, Row, Column, Container, Text } from 'components'

const Hero = styled.div`
  width: 100%;
  height: 60vh;
  color: #fff;
  position: relative;
  margin-top: -90px;
`

const HeroInner = styled.div`
  position: relative;
`

export default ({ isAuthenticated }) =>
  <Hero>
    <Disco
      duration={{ min: 2000, max: 4000 }}
      palletes={['darkmagenta', 'darkblue']}
    >
      <Container>
        <Row justify='center' align='center'>
          <Column text='center' w={1}>
            <HeroInner>
              <Title mt={[200, 220, 240]} mb={[0, 10]} px={[15, 15]}>
                Теперь организовывать конкурсы в социальных сетях – просто и легко!
              </Title>
              {isAuthenticated
                ? (
                  <React.Fragment>
                    <Text fontSize={[2, 3]} p={20}>
                      Попробуйте совершенно бесплатно
                    </Text>
                    <Button view='other' to='/create'>Создать конкурс</Button>
                  </React.Fragment>
                )
                : (
                  <React.Fragment>
                    <Text fontSize={[2, 3]} p={20}>
                      Зарегистрируйтесь и попробуйте совершенно бесплатно
                    </Text>
                    <br />
                    <Button view='other' to='/signup'>Зарегистрироваться</Button>
                  </React.Fragment>
                )
              }
            </HeroInner>
          </Column>
        </Row>
      </Container>
    </Disco>
  </Hero>
