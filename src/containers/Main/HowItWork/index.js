import * as React from 'react'
import { Row, Column, Container, Title, Text } from 'components'

export default () =>
  <Container>
    <Row pt={20} pb={30}>
      <Column w={1}>
        <Title>Как это работает</Title>
        <Text>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit.
          Quos quo iure natus accusantium earum sapiente molestiae doloremque?
          Dolor omnis suscipit laudantium ab maiores provident sapiente! Perferendis aut,
          officia ipsum totam error eos pariatur perspiciatis quaerat animi repellendus
          necessitatibus dignissimos facilis porro unde alias voluptate id molestiae nisi
          asperiores quasi eaque eligendi, deserunt aspernatur, expedita! Incidunt vitae
          consectetur sint earum architecto, non laudantium, excepturi est fugiat officia quia!
          Pariatur itaque, atque aperiam facilis aliquam fugit, eveniet maiores cumque
          doloremque animi suscipit sed ducimus recusandae unde placeat laudantium, optio
          repellendus nam beatae quo, velit! Dignissimos vitae, expedita deserunt officiis
          voluptatum! Nam, fuga.
        </Text>
      </Column>
    </Row>
  </Container>
