// @flow

import * as React from 'react'
import { connect } from 'react-redux'
import Hero from './Hero'
import Features from './Features'
import HowItWork from './HowItWork'

type Props = {
  auth: Object
}

const Main = ({ auth }: Props) =>
  <React.Fragment>
    <Hero isAuthenticated={auth.isAuthenticated} />
    <Features />
    <HowItWork />
  </React.Fragment>

const mapStateToProps = ({ auth }: { auth: Object }): Object => ({
  auth
})


export default connect(mapStateToProps)(Main)
