// @flow

import * as React from 'react'
import { connect } from 'react-redux'
import { Redirect, Link } from 'react-router-dom'
import { get } from 'lodash'
import { Formik, Form } from 'formik'
import yup from 'yup'
import queryString from 'query-string'
import * as actions from 'redux/actions/auth'
import { Flex, Box } from 'grid-styled'
import { API_URL } from 'utils'
import {
  Button,
  Section,
  Field,
  Container,
  Row,
  Column,
  Title,
  Text,
  SocialIcon
} from 'components'

const validationSchema = yup.object({
  email: yup
    .string()
    .email('Некорректный e-mail')
    .required('Обязательное поле для заполнения'),
  password: yup
    .string()
    .required('Обязательное поле для заполнения')
})

type PropsTypes = {
  auth: Object,
  location: Object,
  signin: Function,
  signinFromSocialNetwork: Function
}

class SignIn extends React.Component<PropsTypes, void> {
  componentDidMount () {
    const { token, id } = queryString.parse(this.props.location.search)
    if (token && id) {
      this.props.signinFromSocialNetwork({
        token,
        id,
        rememberMe: true
      })
    }
  }
  render () {
    const { auth, signin, location } = this.props
    if (auth.isAuthenticated) {
      return <Redirect to={`/users/${auth.id}`} />
    }
    return (
      <Container>
        <Row justify='center'>
          <Column w={[1, 1, 3/4, 2/4]} mt={[15, 30]}>
            <Section px={[20, 30, 60]} pt={[5, 10]} pb={[30, 40, 80]}>
              <Title mb={[15, 30]}>Войти</Title>
              <Formik
                onSubmit={(values: Object) => signin(values)}
                validationSchema={validationSchema}
                initialValues={{
                  email: get(location, 'state.email', ''),
                  password: '',
                  rememberMe: true
                }}
              >
                <Form>
                  <Row>
                    <Column w={1}>
                      <Field.Input
                        name='email'
                        placeholder='Введите почту'
                        label='Почта'
                      />
                    </Column>
                  </Row>
                  <Row mt={15}>
                    <Column w={1}>
                      <Field.Input
                        type='password'
                        name='password'
                        placeholder='Введите пароль'
                        label='Пароль'
                      />
                    </Column>
                  </Row>
                  <Row mt={15}>
                    <Column w={1}>
                      <Field.Checkbox
                        name='rememberMe'
                        label='Запомнить меня'
                      />
                    </Column>
                  </Row>
                  <Row mt={[15, 30]} align='center'>
                    <Column w={[1, 3/5]} order={[2, 1]} mt={[15, 0]}>
                      <Text>
                        Вы ещё не зарегистрированы? <br />
                        <Link to='/signup'>Регистрация</Link>
                      </Text>
                    </Column>
                    <Column w={[1, 2/5]} text='right' order={[1, 2]}>
                      <Button
                        type='submit'
                        disabled={auth.isLoading}
                        w={1}
                      >
                        Войти
                      </Button>
                    </Column>
                  </Row>
                </Form>
              </Formik>
            </Section>
            <Section px={[20, 30, 60]} mt={[15]} py={[15, 40]}>
              <Row>
                <Column w={[1, 1/2, 1/2]} py={[15, 15, 0, 0]}>
                  <Flex align='center' is='a' href={`${API_URL}/api/auth/twitter`}>
                    <SocialIcon size='2x' bgSize={50} name='twitter' />
                    <Box pl={15}>
                      Войти через Twitter
                    </Box>
                  </Flex>
                </Column>
                <Column w={[1, 1/2, 1/2]} py={[15, 15, 0, 0]}>
                  <Flex align='center' is='a' href={`${API_URL}/api/auth/instagram`}>
                    <SocialIcon size='2x' bgSize={50} name='instagram' />
                    <Box pl={15}>
                      Войти через Instagram
                    </Box>
                  </Flex>
                </Column>
              </Row>
            </Section>
          </Column>
        </Row>
      </Container>
    )
  }
}

const mapStateToProps = ({ auth, users }: Object, ownProps) => {
  const signupUserId = get(ownProps, 'location.state.signupUserId')
  return {
    auth: {
      ...auth,
      data: users.byId[signupUserId]
    },
  }
}

const mapDispatchToProps = (dispatch: Function) => ({
  signin: (values: Object) => dispatch(actions.signin(values)),
  signinFromSocialNetwork: (values: Object) => dispatch(actions.signinFromSocialNetwork(values))
})

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)
