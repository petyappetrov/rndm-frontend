// @flow

import * as React from 'react'
import { connect } from 'react-redux'
import { Redirect, Link } from 'react-router-dom'
import { Formik, Form } from 'formik'
import { omit } from 'lodash'
import yup from 'yup'
import { Flex, Box } from 'grid-styled'
import { testConfirmPassword, API_URL } from 'utils'
import * as actions from 'redux/actions/auth'
import {
  Button,
  Field,
  Section,
  SocialIcon,
  Column,
  Container,
  Row,
  Title,
  Text
} from 'components'

const validationSchema = yup.object({
  email: yup
    .string()
    .email('Некорректный e-mail')
    .required('Обязательное поле для заполнения'),
  name: yup
    .string()
    .required('Обязательное поле для заполнения'),
  password: yup
    .string()
    .required('Обязательное поле для заполнения'),
  passwordConfirm: yup
    .string()
    .required('Обязательное поле для заполнения')
    .test(
      'passwords-match',
      'Пароли не совпадают. Проверьте идентичность паролей в обоих полях',
      testConfirmPassword
    )
})

type PropsTypes = {
  auth: Object,
  signup: Function
}

const SignUp = ({
  auth,
  signup
}: PropsTypes) => {
  if (auth.isAuthenticated) {
    return <Redirect to='/profile' />
  }
  return (
    <Container>
      <Row justify='center'>
        <Column w={[1, 1, 3/4, 2/4]} mt={[15, 30]}>
          <Section px={[20, 30, 60]} pt={[5, 10]} pb={[30, 40]}>
            <Title mb={[15, 30]}>Регистрация</Title>
            <Formik
              onSubmit={(values: Object) => signup(omit(values, 'passwordConfirm'))}
              validationSchema={validationSchema}
              initialValues={{
                email: '',
                name: '',
                about: '',
                password: '',
                passwordConfirm: ''
              }}
            >
              <Form>
                <Row>
                  <Column w={1}>
                    <Field.Input
                      name='email'
                      placeholder='Введите почту'
                      label='Почта'
                    />
                  </Column>
                </Row>
                <Row mt={15}>
                  <Column w={1}>
                    <Field.Input
                      name='name'
                      placeholder='Введите имя'
                      label='Имя'
                    />
                  </Column>
                </Row>
                <Row mt={15}>
                  <Column w={1}>
                    <Field.Input
                      type='password'
                      name='password'
                      placeholder='Введите пароль'
                      label='Придумайте пароль'
                    />
                  </Column>
                </Row>
                <Row mt={15}>
                  <Column w={1}>
                    <Field.Input
                      type='password'
                      name='passwordConfirm'
                      placeholder='Введите пароль'
                      label='Повторите пароль'
                    />
                  </Column>
                </Row>
                <Row mt={15}>
                  <Column w={1}>
                    <Field.Textarea
                      label='Расскажите о себе (по желанию)'
                      name='about'
                      placeholder='Введите текст'
                    />
                  </Column>
                </Row>
                <Row mt={[15, 30]} align='center'>
                  <Column w={[1, 3/5]} order={[2, 1]} mt={[15, 0]}>
                    <Text>
                      Вы уже зарегистрированы? <br />
                      <Link to='/signin'>Войти</Link>
                    </Text>
                  </Column>
                  <Column w={[1, 2/5]} text='right' order={[1, 2]}>
                    <Button
                      type='submit'
                      disabled={auth.isLoading}
                      w={1}
                    >
                      Регистрация
                    </Button>
                  </Column>
                  <Column w={1} order={3} mt={30} text='center'>
                    <Text fontSize='14px'>
                      Нажимая кнопнку &laquo;Регистрация&raquo;,
                      вы&nbsp;соглашаетесь<br />
                      <Link to='/terms'>с&nbsp;&laquo;Условиями использования&raquo;</Link>
                    </Text>
                  </Column>
                </Row>
              </Form>
            </Formik>
          </Section>
          <Section px={[20, 30, 60]} mt={[15]} py={[15, 40]}>
            <Row>
              <Column w={[1, 1/2, 1/2]} py={[15, 15, 0, 0]}>
                <Flex align='center' is='a' href={`${API_URL}/api/auth/twitter`}>
                  <SocialIcon size='2x' bgSize={50} name='twitter' />
                  <Box pl={15}>
                    Войти через Twitter
                  </Box>
                </Flex>
              </Column>
              <Column w={[1, 1/2, 1/2]} py={[15, 15, 0, 0]}>
                <Flex align='center' is='a' href={`${API_URL}/api/auth/instagram`}>
                  <SocialIcon size='2x' bgSize={50} name='instagram' />
                  <Box pl={15}>
                    Войти через Instagram
                  </Box>
                </Flex>
              </Column>
            </Row>
          </Section>
        </Column>
      </Row>
    </Container>
  )
}

const mapStateToProps = ({ auth }: Object) => ({
  auth
})

const mapDispatchToProps = (dispatch: Function) => ({
  signup: (values) => dispatch(actions.signup(values))
})

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)
