import React from 'react'
import { Column, Row, Text } from 'components'

export default () => (
  <Row>
    <Column w={1}>
      <Text>
        <Text>
          <strong>Вступление</strong>
        </Text>
        Наименование Стороны, именуемый(ая) в&nbsp;дальнейшем Правообладатель,
        адресует настоящее Соглашение (далее по&nbsp;тексту&nbsp;&mdash;
        Соглашение) любому лицу (неопределенному кругу лиц), выразившему
        готовность заключить договор на&nbsp;изложенных ниже условиях (далее
        по&nbsp;тексту&nbsp;&mdash; Пользователь). Данное Соглашение,
        согласно&nbsp;п.&nbsp;2&nbsp;ст.&nbsp;437 Гражданского кодекса
        Российской Федерации, является публичной офертой, принятием условий
        (акцептом) которой является совершение действий, предусмотренных
        Соглашением.
        <br />
        Данное Соглашение, согласно&nbsp;п.&nbsp;2&nbsp;ст.&nbsp;437
        Гражданского кодекса Российской Федерации, является публичной офертой,
        принятием условий (акцептом) которой является совершение действий,
        предусмотренных Соглашением.
        <br />
        <Text mt={15}>
          <strong>1. Определения</strong>
        </Text>
        1.1. Условия Соглашения регулируют отношения Правообладателя
        и&nbsp;Пользователя и&nbsp;содержат следующие определения:
        <br />
        1.1.1. Оферта&nbsp;&mdash; настоящий документ (Соглашение), размещенный
        в&nbsp;сети Интернет по&nbsp;адресу: http://giveaways.su.
        <br />
        1.1.2. Акцепт&nbsp;&mdash; полное и&nbsp;безоговорочное принятие оферты
        путем осуществления действий, указанных
        в&nbsp;п.&nbsp;3.1&nbsp;Соглашения.
        <br />
        1.1.3. Правообладатель&nbsp;&mdash; физическое лицо (Наименование
        Стороны), разместивший(ая) оферту.
        <br />
        1.1.4. Пользователь&nbsp;&mdash; юридическое или дееспособное физическое
        лицо, заключившее Соглашение посредством акцепта на&nbsp;условиях,
        содержащихся в&nbsp;оферте.
        <br />
        1.1.5. Генератор конкурсов&nbsp;&mdash; совокупность веб-страниц,
        размещенных на&nbsp;виртуальном сервере и&nbsp;образующих единую
        структуру, расположенных в&nbsp;сети Интернет по&nbsp;адресу:
        http://giveaways.su (далее по&nbsp;тексту&nbsp;&mdash; Сайт).
        <br />
        1.1.6. Контент&nbsp;&mdash; информация, представленная в&nbsp;текстовом
        и&nbsp;графическом форматах на&nbsp;Сайте, являющаяся его наполнением.
        Контент Сайта распределяется на&nbsp;основной&nbsp;&mdash;
        пользовательский, и&nbsp;вспомогательный&nbsp;&mdash; административный,
        который создает Правообладатель для облегчения функционирования Сайта,
        включая интерфейс Сайта.
        <br />
        1.1.7. Простая (неисключительная) лицензия&nbsp;&mdash; неисключительное
        право Пользователя использовать результат интеллектуальной деятельности,
        указанный в&nbsp;п.&nbsp;2.1&nbsp;Соглашения, с&nbsp;сохранением
        за&nbsp;Правообладателем права выдачи лицензий другим лицам.
        <br />
        1.1.8. Личный кабинет&nbsp;&mdash; это виртуальный инструмент
        персонального самообслуживания Правообладателя, расположенный
        на&nbsp;официальном интернет&nbsp;&mdash; сайте: http://giveaways.su.
        <br />
        1.1.9. Личная учетная запись Пользователя&nbsp;&mdash; уникальный логин
        и&nbsp;пароль для входа в&nbsp;личный кабинет.
        <br />
        <Text mt={15}>
          <strong>2. Предмет Соглашения</strong>
        </Text>
        2.1. Настоящее Соглашение определяет условия и&nbsp;порядок
        использования результатов интеллектуальной деятельности, в&nbsp;том
        числе элементов контента Веб-сайта, расположенного в&nbsp;сети Интернет
        по&nbsp;адресу: http://giveaways.su (далее по&nbsp;тексту&nbsp;&mdash;
        Сайт), ответственность Сторон и&nbsp;другие особенности функционирования
        Сайта и&nbsp;взаимоотношений Пользователей Сайта
        с&nbsp;Правообладателем, а&nbsp;также друг с&nbsp;другом.
        <br />
        2.2. Правообладатель гарантирует, что он&nbsp;является правообладателем
        исключительных прав на&nbsp;Сайт, указанный
        в&nbsp;п.&nbsp;2.1&nbsp;Соглашения.
        <br />
        <Text mt={15}>
          <strong>3. Согласие с&nbsp;условиями соглашения</strong>
        </Text>
        3.1. Акцептом (принятием оферты) является регистрация личного кабинета
        Пользователя.
        <br />
        3.2. Совершая действия по&nbsp;принятию оферты в&nbsp;порядке,
        определенном&nbsp;п.&nbsp;3.1&nbsp;Соглашения, Пользователь гарантирует,
        что ознакомлен, соглашается, полностью и&nbsp;безоговорочно принимает
        все условия Соглашения, обязуется их&nbsp;соблюдать.
        <br />
        3.3. Настоящим Пользователь подтверждает, что акцепт (совершение
        действий по&nbsp;принятию оферты) равносилен подписанию
        и&nbsp;заключению Соглашения на&nbsp;условиях, изложенных
        в&nbsp;настоящем Соглашении.
        <br />
        3.4. Оферта вступает в&nbsp;силу с&nbsp;момента размещения в&nbsp;сети
        Интернет по&nbsp;адресу{' '}
        <a href='http://giveaways.su'>http://giveaways.su</a> и&nbsp;действует
        до&nbsp;момента отзыва оферты.
        <br />
        3.5. Настоящее Соглашение размещено в&nbsp;письменном виде
        на&nbsp;Сайте. В&nbsp;случае необходимости любому лицу по&nbsp;его
        запросу может быть предоставлена возможность ознакомиться
        с&nbsp;бумажной версией Соглашения в&nbsp;офисе Правообладателя.
        <br />
        3.6. Соглашение может быть принято исключительно в&nbsp;целом
        (п.&nbsp;1&nbsp;ст.&nbsp;428 Гражданского кодекса Российской Федерации).
        После принятия Пользователем условий настоящего Соглашения оно
        приобретает силу договора, заключенного между Правообладателем
        и&nbsp;Пользователем, при этом такой договор как бумажный документ,
        подписанный обеими Сторонами, не&nbsp;оформляется.
        <br />
        3.7. Для организации взаимодействия между Правообладателем
        и&nbsp;Пользователем самостоятельно регистрирует Личный кабинет
        в&nbsp;следующем порядке: Переход на&nbsp;страницу регистрации,
        заполнение личных данных, отправка данных.
        <br />
        3.8. В&nbsp;Личном кабинете Пользователя указывается следующая
        информация о&nbsp;Пользователе: Имя пользователя, электронная почта,
        привязанные социальные аккаунты, короткий текст о&nbsp;себе.
        <br />
        3.9. Правообладатель оставляет за&nbsp;собой право вносить изменения
        в&nbsp;настоящее Соглашение без какого-либо специального уведомления,
        в&nbsp;связи с&nbsp;чем Пользователь обязуется регулярно отслеживать
        изменения в&nbsp;Соглашении. Новая редакция Соглашения вступает
        в&nbsp;силу с&nbsp;момента ее&nbsp;размещения на&nbsp;данной странице,
        если иное не&nbsp;предусмотрено новой редакцией Соглашения. Действующая
        редакция Соглашения всегда находится на&nbsp;данной странице
        по&nbsp;адресу:{' '}
        <a href='http://giveaways.su/terms'>http://giveaways.su/terms</a>.
        <br />
        <Text mt={15}>
          <strong>4. Права и&nbsp;обязанности сторон</strong>
        </Text>
        4.1. Правообладатель обязуется:
        <br />
        4.1.1. В&nbsp;течение Срок календарных дней со&nbsp;дня получения
        соответствующего письменного уведомления Пользователя своими силами
        и&nbsp;за&nbsp;свой счет устранить выявленные Пользователем недостатки
        Сайта, а&nbsp;именно:
        <br />
        &mdash;&nbsp;несоответствие содержания Сайта данным, указанным
        в&nbsp;п.&nbsp;2.1&nbsp;Соглашения;
        <br />
        &mdash;&nbsp;наличие в&nbsp;составе Сайта материалов, запрещенных
        к&nbsp;распространению законодательством.
        <br />
        4.1.2. Воздерживаться от&nbsp;каких-либо действий, способных затруднить
        осуществление Пользователем предоставленного ему права использования
        Сайта в&nbsp;установленных Соглашением пределах.
        <br />
        4.1.3. Предоставлять информацию по&nbsp;вопросам работы с&nbsp;Сайтом
        посредством электронной почты, форума, блога. Актуальные адреса
        электронной почты находятся в&nbsp;подвале Сайта по&nbsp;адресу:{' '}
        <a href='http://giveaways.su'>http://giveaways.su</a>.
        <br />
        4.1.4. Использовать все личные данные и&nbsp;иную конфиденциальную
        информацию о&nbsp;Пользователе только для оказания услуг
        в&nbsp;соответствии с&nbsp;Соглашением, не&nbsp;передавать третьим лицам
        находящуюся у&nbsp;него документацию и&nbsp;информацию
        о&nbsp;Пользователе.
        <br />
        4.1.5. Обеспечивать конфиденциальность информации, введенной
        Пользователем при использовании Сайта через личную учетную запись
        Пользователя, за&nbsp;исключением случаев размещения такой информации
        в&nbsp;общедоступных разделах Сайта (например, чат).
        <br />
        4.1.6. Консультировать Пользователя по&nbsp;всем вопросам, касающимся
        Сайта. Сложность вопроса, объем, и&nbsp;сроки консультирования
        определяются в&nbsp;каждом конкретном случае Правообладателем
        самостоятельно.
        <br />
        4.2. Пользователь обязуется:
        <br />
        4.2.1. Использовать Сайт только в&nbsp;пределах тех прав и&nbsp;теми
        способами, которые предусмотрены в&nbsp;Соглашении.
        <br />
        4.2.2. При регистрации в&nbsp;личном кабинете предоставить реальные,
        а&nbsp;не&nbsp;вымышленные сведения. В&nbsp;случае обнаружения
        недостоверности представленных сведений, а&nbsp;также если
        у&nbsp;Правообладателя возникнут обоснованные сомнения
        в&nbsp;их&nbsp;достоверности (в&nbsp;том числе, если при попытке
        связаться указанные контактные данные окажутся несуществующими),
        Правообладатель имеет право в&nbsp;одностороннем порядке прекратить
        отношения с&nbsp;Пользователем, удалить учётную запись Пользователя
        и&nbsp;заблокировать доступ на&nbsp;Сайт.
        <br />
        4.2.3. Хранить в&nbsp;тайне и&nbsp;не&nbsp;раскрывать третьим лицам
        информацию о&nbsp;своем пароле, дающем доступ в&nbsp;Личный кабинет
        Пользователя. В&nbsp;случае, если такая информация по&nbsp;тем или иным
        причинам станет известна третьим лицам, Пользователь обязуется
        немедленно изменить его.
        <br />
        4.2.4. Строго придерживаться и&nbsp;не&nbsp;нарушать условий Соглашения,
        а&nbsp;также обеспечить конфиденциальность полученной при сотрудничестве
        с&nbsp;Правообладателем коммерческой и&nbsp;технической информации.
        <br />
        4.2.5. Воздерживаться от&nbsp;копирования в&nbsp;любой форме,
        а&nbsp;также от&nbsp;изменения, дополнения, распространения Сайта,
        контента Сайта (либо любой его части), а&nbsp;также воздерживаться
        от&nbsp;создания на&nbsp;его основе производных объектов без
        предварительного письменного разрешения Правообладателя.
        <br />
        4.2.6. Не&nbsp;использовать никаких приборов либо компьютерных программ
        для вмешательства или попытки вмешательства в&nbsp;процесс нормального
        функционирования Сайта Правообладателя.
        <br />
        4.2.7. Незамедлительно информировать Правообладателя обо всех ставших
        ему известных фактах противоправного использования Сайта третьими лицами
        .
        <br />
        4.2.8. Использовать Сайт, не&nbsp;нарушая имущественных и/или личных
        неимущественных прав третьих лиц, а&nbsp;равно запретов
        и&nbsp;ограничений, установленных применимым правом, включая без
        ограничения: авторские и&nbsp;смежные права, права на&nbsp;товарные
        знаки, знаки обслуживания и&nbsp;наименования мест происхождения
        товаров, права на&nbsp;промышленные образцы, права на&nbsp;использование
        изображений людей.
        <br />
        4.2.9. Не&nbsp;допускать размещение и&nbsp;передачу материалов
        незаконного, неприличного, клеветнического, дискредитирующего,
        угрожающего, порнографического, враждебного характера, а&nbsp;также
        содержащих домогательства и&nbsp;признаки расовой или этнической
        дискриминации, призывающих к&nbsp;совершению действий, которые могут
        считаться уголовным преступлением или являться нарушением какого-либо
        законодательства, равно как и&nbsp;считаться недопустимыми по&nbsp;иным
        причинам, материалов, пропагандирующих культ насилия и&nbsp;жестокости,
        материалов, содержащих нецензурную брань.
        <br />
        4.2.10. Не&nbsp;распространять рекламные материалы в&nbsp;личных
        сообщениях иным Пользователям без получения их&nbsp;предварительного
        согласия на&nbsp;получение таких материалов (СПАМ).
        <br />
        4.2.11. Исполнять иные обязанности, предусмотренные Соглашением.
        <br />
        4.3. Правообладатель вправе:
        <br />
        4.3.1. Приостановить или прекратить регистрацию и&nbsp;доступ
        Пользователя на&nbsp;Сайт, если Правообладатель будет обоснованно
        считать, что Пользователь ведет неправомерную деятельность.
        <br />
        4.3.2. Собирать информацию о&nbsp;предпочтениях Пользователей
        и&nbsp;способах использования ими Сайта (наиболее часто используемые
        функции, настройки, предпочитаемое время и&nbsp;продолжительность работы
        с&nbsp;Сайтом и&nbsp;прочее), которая не&nbsp;является персональными
        данными, для улучшения работы Сайта, диагностики и&nbsp;профилактики
        сбоев Сайта.
        <br />
        4.3.3. Запросить у&nbsp;Пользователя дополнительную информацию
        о&nbsp;его должности и&nbsp;роде деятельности, в&nbsp;том числе
        о&nbsp;компании, которую он&nbsp;представляет, при регистрации
        в&nbsp;личном кабинете или в&nbsp;любой другой момент. Правообладатель
        вправе отказать Пользователю в&nbsp;использовании Сайта (вплоть
        до&nbsp;удаления Личной учетной записи Пользователя) или ограничить его
        использование по&nbsp;собственному усмотрению, если
        он&nbsp;не&nbsp;является сотрудником компании или представителем лица,
        для которых предназначен Сайт, или если Пользователь откажется
        предоставить запрошенные сведения. Правообладатель по&nbsp;собственному
        усмотрению вправе предоставлять ограниченный доступ в&nbsp;целях
        ознакомления к&nbsp;Сайту иным лицам, не&nbsp;осуществляющим профильную
        деятельность.
        <br />
        4.3.4. Вносить в&nbsp;одностороннем порядке изменения в&nbsp;Соглашение
        путем издания его новых редакций.
        <br />
        4.3.5. Удалять пользовательский контент по&nbsp;требованию
        уполномоченных органов или заинтересованных лиц в&nbsp;случае, если
        данный контент нарушает применимое законодательство или права третьих
        лиц.
        <br />
        4.3.6. Временно прекращать работу Сайта, а&nbsp;равно частично
        ограничивать или полностью прекращать доступ к&nbsp;Сайту
        до&nbsp;завершения необходимого технического обслуживания&nbsp;и (или)
        модернизации Сайта. Пользователь не&nbsp;вправе требовать возмещения
        убытков за&nbsp;такое временное прекращение оказания услуг или
        ограничение доступности Сайта.
        <br />
        4.4. Пользователь вправе:
        <br />
        4.4.1. Использовать Сайт в&nbsp;пределах и&nbsp;способами,
        предусмотренными Соглашением.
        <br />
        4.5. Пользователь не&nbsp;вправе давать согласие на&nbsp;выполнение
        настоящего Соглашения в&nbsp;случаях, если у&nbsp;него нет законного
        права использовать Сайт в&nbsp;стране, в&nbsp;которой находится или
        проживает, или если он&nbsp;не&nbsp;достиг возраста, с&nbsp;которого
        имеет право заключать данное Соглашение.
        <br />
        <Text mt={15}>
          <strong>5. Условия и&nbsp;порядок использования</strong>
        </Text>
        5.1. При условии выполнения Пользователем настоящего Соглашения,
        Пользователю предоставляется простая (неисключительная) лицензия
        на&nbsp;использование Сайта с&nbsp;помощью персонального компьютера,
        мобильного телефона или другого устройства, в&nbsp;объеме
        и&nbsp;порядке, установленном Соглашением, без права предоставления
        сублицензий и&nbsp;переуступки.
        <br />
        5.2. В&nbsp;соответствии с&nbsp;условиями Соглашения Правообладатель
        предоставляет Пользователю право использования Сайта следующими
        способами:
        <br />
        5.2.1. Использовать Сайт для просмотра, ознакомления, оставления
        комментариев и&nbsp;иных записей и&nbsp;реализации иного функционала
        Сайта, в&nbsp;том числе путем воспроизведения на&nbsp;мониторе (экране)
        соответствующего технического средства Пользователя;
        <br />
        5.2.2. Кратковременно загружать в&nbsp;память ЭВМ для целей
        использования Сайта и&nbsp;его функционала;
        <br />
        5.2.3. Цитировать элементы пользовательского контента Сайта
        с&nbsp;указанием источника цитирования, включающего ссылку
        на&nbsp;URL-адрес Сайта.
        <br />
        5.2.4. Способ использования: Способ использования.
        <br />
        5.3. Пользователь не&nbsp;вправе предпринимать указанные ниже действия
        при использовании Сайта, а&nbsp;равно любых составных частей Сайта:
        <br />
        5.3.1. Модифицировать или иным образом перерабатывать Сайт, в&nbsp;том
        числе, осуществлять перевод на&nbsp;другие языки.
        <br />
        5.3.2. Копировать, распространять или перерабатывать материалы
        и&nbsp;сведения, содержащиеся на&nbsp;Сайте, за&nbsp;исключением
        случаев, когда это необходимо и&nbsp;вызвано реализацией функционала,
        доступного как конкретному Пользователю.
        <br />
        5.3.3. Нарушать целостность защитной системы или осуществлять какие-либо
        действия, направленные на&nbsp;обход, снятие или деактивацию технических
        средств защиты; использовать какие-либо программные коды,
        предназначенные для искажения, удаления, повреждения, имитации или
        нарушения целостности Сайта, передаваемой информации или протоколов.
        <br />
        5.4. Любые права, не&nbsp;предоставленные Пользователю в&nbsp;явной
        форме в&nbsp;соответствии с&nbsp;настоящим Соглашением, сохраняются
        за&nbsp;Правообладателем.
        <br />
        5.5. Сайт предоставляется Правообладателем в&nbsp;состоянии &laquo;Как
        есть&raquo; (&laquo;AS&nbsp;IS&raquo;), без гарантийных обязательств
        Правообладателя или какой-либо обязанности по&nbsp;устранению
        недостатков, эксплуатационной поддержке и&nbsp;усовершенствованию.
        <br />
        5.6. В&nbsp;отношении пользовательского контента Пользователь
        гарантирует, что является владельцем или обладает необходимыми
        лицензиями, правами, согласием и&nbsp;разрешениями на&nbsp;использование
        и&nbsp;предоставление Правообладателю права использовать весь
        пользовательский контент в&nbsp;соответствии с&nbsp;настоящим
        Соглашением; у&nbsp;него имеется письменное согласие&nbsp;и (или)
        разрешение каждого лица, так или иначе присутствующего
        в&nbsp;пользовательском контенте, использовать персональные данные
        (включая изображение при необходимости) этого лица для того, чтобы
        размещать и&nbsp;использовать пользовательский контент способом,
        предусмотренным настоящим Соглашением.
        <br />
        5.7. Принимая условия настоящего Соглашения, Пользователь безвозмездно
        предоставляет Правообладателю и&nbsp;другим Пользователям
        неисключительное безвозмездное право использования (простую лицензию)
        материалов, которые Пользователь добавляет (размещает) на&nbsp;Сайте
        в&nbsp;разделах, предназначенных для доступа всех или части
        Пользователей (чаты, обсуждения, комментарии и&nbsp;прочее). Указанные
        право и/или разрешение на&nbsp;использование материалов предоставляются
        одновременно с&nbsp;добавлением Пользователем таких материалов
        на&nbsp;Сайт на&nbsp;весь срок действия исключительных прав
        на&nbsp;объекты интеллектуальной собственности или защиты
        неимущественных прав на&nbsp;указанные материалы для
        их&nbsp;использования на&nbsp;территории всех стран мира.
        <br />
        <Text mt={15}>
          <strong>
            6. Персональные данные и&nbsp;политика конфиденциальности
          </strong>
        </Text>
        6.1. Для выполнения условий Соглашения Пользователь соглашается
        предоставить и&nbsp;дает согласие на&nbsp;обработку персональных данных
        в&nbsp;соответствии с&nbsp;Федеральным законом от&nbsp;27.07.2006 года
        &#8470;&nbsp;152-ФЗ &laquo;О&nbsp;персональных данных&raquo;
        на&nbsp;условиях и&nbsp;для целей надлежащего исполнения Соглашения. Под
        &laquo;персональными данными&raquo; понимается персональная информация,
        которую Пользователь предоставляет о&nbsp;себе самостоятельно для
        совершения акцепта.
        <br />
        6.2. В&nbsp;случаях утери Пользователем идентификационных данных
        (логина/пароля) для доступа к&nbsp;личному кабинету, Пользователь вправе
        запросить данные сведения у&nbsp;Правообладателя, посредством
        направления запроса через электронную почту Правообладателя.
        <br />
        6.3. Полученная Правообладателем информация (персональные данные)
        не&nbsp;подлежит разглашению, за&nbsp;исключением случаев, когда
        ее&nbsp;раскрытие является обязательным по&nbsp;законодательству
        Российской Федерации или необходимо для работы Сайта и&nbsp;его функций
        (например, при публикации комментариев в&nbsp;разделе Сайта
        &laquo;Комментарии&raquo; под написанным Пользователем комментарием
        отображаются имя, дата и&nbsp;время отправки комментария).
        <br />
        <Text mt={15}>
          <strong>7. Ответственность сторон</strong>
        </Text>
        7.1. Стороны несут ответственность за&nbsp;неисполнение или ненадлежащее
        исполнение своих обязательств в&nbsp;соответствии с&nbsp;условиями
        Соглашения и&nbsp;законодательством России.
        <br />
        7.2. Правообладатель не&nbsp;принимает на&nbsp;себя ответственность
        за&nbsp;соответствие Сайта целям использования.
        <br />
        7.3. Правообладатель не&nbsp;несет ответственности за&nbsp;технические
        перебои в&nbsp;работе Сайта. Вместе с&nbsp;тем Правообладатель обязуется
        принимать все разумные меры для предотвращения таких перебоев.
        <br />
        7.4. Правообладатель не&nbsp;несет ответственности за&nbsp;любые
        действия Пользователя, связанные с&nbsp;использованием предоставленных
        прав использования Сайта; за&nbsp;ущерб любого рода, понесенный
        Пользователем из-за утери и/или разглашения своих данных либо
        в&nbsp;процессе использования Сайта.
        <br />
        7.5. В&nbsp;случае если какое-либо третье лицо предъявляет
        Правообладателю претензию в&nbsp;связи с&nbsp;нарушением Пользователем
        Соглашения либо действующих законодательных норм, нарушением
        Пользователем прав третьих лиц (в&nbsp;том числе прав
        на&nbsp;интеллектуальную собственность), Пользователь обязуется
        компенсировать Правообладателю все расходы и&nbsp;потери, в&nbsp;том
        числе оплатить любые компенсации и&nbsp;прочие затраты, связанные
        с&nbsp;такой претензией.
        <br />
        7.6. Правообладатель не&nbsp;несет ответственности за&nbsp;содержание
        сообщений или материалов Пользователей Сайта (пользовательский контент),
        любые мнения, рекомендации или советы, содержащиеся в&nbsp;таком
        контенте. Правообладатель не&nbsp;осуществляет предварительную проверку
        содержания, подлинности и&nbsp;безопасности этих материалов либо
        их&nbsp;компонентов, а&nbsp;равно их&nbsp;соответствия требованиям
        применимого права, и&nbsp;наличия у&nbsp;Пользователей необходимого
        объема прав на&nbsp;их&nbsp;использование в&nbsp;обязательном порядке.
        <br />
        <Text mt={15}>
          <strong>8. Разрешение споров</strong>
        </Text>
        8.1. Претензионный порядок досудебного урегулирования споров, вытекающих
        из&nbsp;настоящего Соглашения, является для Сторон обязательным.
        <br />
        8.2. Претензионные письма направляются Сторонами нарочным либо заказным
        почтовым отправлением с&nbsp;уведомлением о&nbsp;вручении по&nbsp;адресу
        местонахождения Стороны.
        <br />
        8.3. Направление Сторонами претензионных писем иным способом, чем
        указано в&nbsp;п.&nbsp;8.2&nbsp;Соглашения, не&nbsp;допускается.
        <br />
        8.4. Срок рассмотрения претензионного письма составляет Срок
        рассмотрения рабочих дней со&nbsp;дня получения последнего адресатом.
        <br />
        8.5. Споры по&nbsp;настоящему Соглашению разрешаются в&nbsp;судебном
        порядке в&nbsp;Арбитражном суде&nbsp;г. Якутска.
        <br />
        <Text mt={15}>
          <strong>9. Заключительные положения</strong>
        </Text>
        9.1. Настоящее Соглашение регулируется и&nbsp;толкуется
        в&nbsp;соответствии с&nbsp;законодательством Российской Федерации.
        Вопросы, не&nbsp;урегулированные настоящим Соглашением, подлежат
        разрешению в&nbsp;соответствии с&nbsp;законодательством Российской
        Федерации. Все возможные споры, вытекающие из&nbsp;отношений,
        регулируемых настоящим Соглашением, разрешаются в&nbsp;порядке,
        установленном действующим законодательством Российской Федерации,
        по&nbsp;нормам российского права. Везде по&nbsp;тексту настоящего
        Соглашения под термином &laquo;законодательство&raquo; понимается
        законодательство Российской Федерации.
        <br />
      </Text>
    </Column>
  </Row>
)
