import React from 'react'
import { Switch, Route, NavLink } from 'react-router-dom'
import { map } from 'lodash'
import { Row, Container, Column, Title, Section } from 'components'
import styled from 'styled-components'
import { withWindowWidth } from 'utils'
import Privacy from './Privacy'
import Terms from './Terms'
import Faq from './Faq'
import Rules from './Rules'

const TABS = {
  '/rules': 'Условия и правила публикации',
  '/faq': 'Часто задаваемые вопросы',
  '/terms': 'Условия использования',
  '/privacy': 'Политика конфиденциальности',
}

const StyledLink = styled(NavLink)`
  display: flex;
  padding: 15px 15px 15px 20px;
  border-bottom: 1px solid #dadada;
  position: relative;
  color: #000;
  &:last-child {
    border-bottom: none;
  }
  &.active {
    color: #000;
    pointer-events: none;
    cursor: default;
    font-weight: 700;
    &::before {
      content: '';
      position: absolute;
      left: 0;
      top: 0;
      width: 3px;
      height: 100%;
      background: #000;
    }
  }
`

const Statics = ({ match, width }) =>
  <Container>
    <Title>{TABS[match.url]}</Title>
    <Row>
      {width > 768 &&
        <Column w={[1, 1, 1/4]} mb={15}>
          <Section style={{ position: 'sticky', top: 15 }} overflow='hidden' p={[0]}>
            {map(TABS, (label, path) =>
              <StyledLink to={path} key={path}>{label}</StyledLink>
            )}
          </Section>
        </Column>
      }
      <Column w={[1, 1, 3/4]}>
        <Section>
          <Switch>
            <Route path='/terms' component={Terms} />
            <Route path='/privacy' component={Privacy} />
            <Route path='/rules' component={Rules} />
            <Route path='/faq' component={Faq} />
          </Switch>
        </Section>
      </Column>
    </Row>
  </Container>

export default withWindowWidth(Statics)

