// @ flow

import * as React from 'react'
import { Formik, Form } from 'formik'
import yup from 'yup'
import { Field, Button, Modal, Row, Column } from 'components'

type Props = {
  user: Object,
  onCloseModal: Function,
  updateUser: Function,
  removeUser: Function
}

const FormEdit = ({
  onCloseModal,
  updateUser,
  removeUser,
  user,
}: Props) =>
  <Modal
    isOpened
    size='normal'
    onRequestClose={onCloseModal}
    contentLabel='edit-profile-modal'
  >
    <Formik
      validationSchema={yup.object({
        email: yup.string().email('Некорректный e-mail').required('Обязательное поле для заполнения'),
        name: yup.string().required('Обязательное поле для заполнения')
      })}
      initialValues={{
        email: user.email,
        name: user.name,
        about: user.about,
        photo: ''
      }}
      onSubmit={updateUser}
    >
      <Form>
        <Modal.Header>
          <Row>
            <Column w={1}>
              <strong>Редактировать профиль</strong>
            </Column>
          </Row>
        </Modal.Header>
        <Modal.Content>
          <Row>
            <Column w={[1, 1, 1/2]}>
              <Field.Input
                name='name'
                placeholder='Введите имя'
                label='Имя'
              />
            </Column>
            <Column w={[1, 1, 1/2]} mt={[15, 15, 0]}>
              <Field.Input
                name='email'
                placeholder='Введите почту'
                label='Почта'
              />
            </Column>
          </Row>
          <Row mt={15}>
            <Column w={1}>
              <Field.Textarea
                name='about'
                placeholder='Введите текст'
                label='Расскажите о себе (по желанию)'
              />
            </Column>
          </Row>
          <Row mt={15}>
            <Column w={1}>
              <Field.Dropzone
                name='photo'
                label='Загрузить новое фото'
                accept='image/jpeg, image/jpg, image/png'
              />
            </Column>
          </Row>
        </Modal.Content>
        <Modal.Footer>
          <Button
            w={[1, 'auto']}
            order={[2, 1]}
            mr='auto'
            mt={[15, 0]}
            onClick={removeUser}
            view='danger'
          >
            Удалить профиль
          </Button>
          <Button
            w={[1, 'auto']}
            order={[3, 2]}
            mt={[15, 0]}
            mr={[0, 15]}
            view='ghost'
            onClick={onCloseModal}
          >
            Отменить
          </Button>
          <Button
            w={[1, 'auto']}
            order={[1, 3]}
            type='submit'
            disabled={user.loading}
          >
            Редактировать
          </Button>
        </Modal.Footer>
      </Form>
    </Formik>
  </Modal>

export default FormEdit
