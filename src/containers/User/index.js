// @flow

import * as React from 'react'
import { Link, Route } from 'react-router-dom'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash'
import { pure, compose } from 'recompose'
import moment from 'moment'
import { Box, Flex } from 'grid-styled'
import {
  Avatar,
  Button,
  Section,
  Title,
  Row,
  Column,
  Container,
  Table,
  Text,
  Spinner
} from 'components'
import { signout } from 'redux/actions/auth'
import { getUser, removeUser, updateUser } from 'redux/actions/users'
import { selectUser, selectUserContests, selectIsMe } from 'redux/selectors'
import UserEdit from './UserEdit'


type Props = {
  user: Object,
  match: Object,
  isMe: boolean,
  history: Object,
  updateUser: Function,
  removeUser: Function,
  signout: Function,
  getUser: Function,
  contests: Array<Object>,
}


class User extends React.Component<Props> {
  componentWillMount () {
    this.props.getUser(this.props.match.params.id)
  }

  onCloseModal = () => {
    this.props.history.push({
      pathname: this.props.match.url,
      state: {
        jumpToTop: false
      }
    })
  }

  render () {
    const {
      user,
      isMe,
      updateUser,
      removeUser,
      signout,
      contests,
      match,
    } = this.props

    if (isEmpty(user)) {
      return <Flex justify='center'><Spinner /></Flex>
    }

    return (
      <Container>
        <Title>{isMe ? 'Мой профиль' : 'Организатор'}</Title>
        <Row>
          <Column w={[1, 1, 1, 1/3]} pb={15}>
            <Section px={30}>
              <Row justify='center'>
                <Column>
                  <Avatar src={user.photoUrl} />
                </Column>
                <Column text='center' w={1}>
                  <Text fontSize='32px' mt={15}>
                    <strong>{user.name}</strong>
                  </Text>
                  {user.about &&
                    <Text mt={15}>{user.about}</Text>
                  }
                  <Text mt={15}>{user.email}</Text>
                  <Box mt={15}>
                    Привязанные аккаунты:
                    <Text>
                      <a href='http://instagram.com'>instagram</a>
                    </Text>
                  </Box>
                  {isMe &&
                    <Row justify='center' mt={30}>
                      <Column>
                        <Button
                          view='ghost'
                          onClick={signout}
                        >
                          Выйти
                        </Button>
                      </Column>
                      <Column>
                        <Button
                          view='ghost'
                          to={{
                            pathname: `${match.url}/edit`,
                            state: {
                              jumpToTop: false
                            }
                          }}
                        >
                          Редактировать
                        </Button>
                      </Column>
                    </Row>
                  }
                </Column>
              </Row>
            </Section>
          </Column>
          <Column w={[1, 1, 1, 2/3]}>
            <Section>
              <Section.Title>
                {isMe ? 'Мои конкурсы' : 'Конкурсы'}
              </Section.Title>
              {!isEmpty(contests)
                ? (
                  <Table
                    data={contests}
                    columns={[
                      {
                        header: 'Название',
                        accessor: 'title',
                        transform: (col, row) => <Link to={`/contests/${row._id}`}>{col}</Link>
                      },
                      {
                        header: 'Тип',
                        accessor: 'type'
                      },
                      {
                        header: 'Социальная сеть',
                        accessor: 'socialNetwork'
                      },
                      {
                        header: 'Создано',
                        accessor: 'createdAt',
                        transform: col => moment(col).format('DD.MM.YYYY')
                      },
                      {
                        header: 'Конец',
                        accessor: 'endDate',
                        transform: col => moment(col).format('DD.MM.YYYY')
                      }
                    ]}
                  />
                )
                : (
                  <p>
                    У вас пока нет конкурсов.
                    {' '}
                    <Link to='/create'>Создайте свой первый конкурс</Link>
                  </p>
                )
              }
            </Section>
          </Column>
        </Row>
        {isMe &&
          <Route
            path={`${match.url}/edit`}
            render={props =>
              <UserEdit
                {...props}
                updateUser={updateUser}
                removeUser={removeUser}
                user={user}
                onCloseModal={this.onCloseModal}
              />
            }
          />
        }
      </Container>
    )
  }
}


const mapStateToProps = (state: Object, ownProps) => ({
  isMe: selectIsMe(state, ownProps),
  user: selectUser(state, ownProps),
  contests: selectUserContests(state, ownProps)
})


const mapDispatchToProps = {
  signout,
  getUser,
  removeUser,
  updateUser
}


export default compose(
  pure,
  connect(mapStateToProps, mapDispatchToProps)
)(User)
