import { injectGlobal } from 'styled-components'
import styledNormalize from 'styled-normalize'

export default injectGlobal`
  ${styledNormalize}
  html,
  body,
  #root {
    width: 100%;
    height: 100%;
    background: #F2F2F2;
    position: relative;
  }
  #root {
    display: flex;
    min-height: 100vh;
    flex-direction: column;
  }
  *,
  button,
  input,
  optgroup,
  select,
  textarea {
    font-family: 'PT Sans', sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }
  input[type='text'],
  input[type='password'],
  textarea,
  button {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    &:focus {
      border-color: #007eff;
      box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075), 0 0 0 3px rgba(0, 126, 255, 0.1);
      outline: none;
    }
  }
  textarea::placeholder,
  input[type='password']::placeholder,
  input[type='text']::placeholder {
    color: #aaa;
  }
  a {
    color: #3193DF;
    text-decoration: none;
  }
  .DayPicker-Month {
    margin: 0 15px 0 0;
    &:last-child {
      margin: 0;
    }
  }
  .Select-menu-outer {
    z-index: 101;
  }
  .Select-control {
    border: 1px solid #ccc;
  }
`
