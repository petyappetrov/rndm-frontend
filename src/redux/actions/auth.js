// @flow

import constants from 'redux/constants'


export function signin (payload: {
  email: string,
  password: string,
  rememberMe: ?boolean
}): Object {
  return {
    type: constants.AUTH_SIGNIN_REQUEST,
    payload
  }
}


export function signup (payload: {
  email: string,
  password: string,
  name: string
}): Object {
  return {
    type: constants.AUTH_SIGNUP_REQUEST,
    payload
  }
}


export function signout (): Object {
  return {
    type: constants.AUTH_SIGNOUT_REQUEST
  }
}


export function signinFromSocialNetwork ({
  token,
  id,
  rememberMe
}: {
  token: string,
  id: string,
  rememberMe: boolean
}): Object {
  return {
    type: constants.AUTH_FROM_SOCIAL_NETWORK_REQUEST,
    token,
    id,
    rememberMe
  }
}
