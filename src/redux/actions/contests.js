import constants from 'redux/constants'


export function getContests (filters): Object {
  return {
    type: constants.CONTESTS_GET_REQUEST,
    filters
  }
}


export function getContest (id: String): Object {
  return {
    type: constants.CONTEST_GET_REQUEST,
    id
  }
}


export function getUserContests (createdBy: String): Object {
  return {
    type: constants.CONTESTS_USER_GET_REQUEST,
    createdBy
  }
}


export function createContest (payload: Object): Object {
  return {
    type: constants.CONTEST_CREATE_REQUEST,
    payload
  }
}


export function removeContest (id: string): Object {
  return {
    type: constants.CONTEST_REMOVE__REQUEST,
    id
  }
}


export function updateContest (payload): Object {
  return {
    type: constants.CONTEST_UPDATE_REQUEST,
    payload
  }
}
