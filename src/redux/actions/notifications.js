// @flow
import constants from 'redux/constants'

type TypesNotification = {
  title: string,
  message?: string,
  dismiss?: number
}


export function showNotification ({ title, message, dismiss }: TypesNotification): Object {
  return {
    type: constants.NOTIFICATION_ADD,
    notification: {
      title,
      message,
      dismiss,
      id: new Date().getTime()
    }
  }
}


export function hideNotification (id: string): Object {
  return {
    type: constants.NOTIFICATION_REMOVE,
    id
  }
}
