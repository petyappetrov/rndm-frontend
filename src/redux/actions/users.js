import constants from 'redux/constants'


export function getUser (id: string): Object {
  return {
    type: constants.USER_GET_REQUEST,
    id
  }
}


export function updateUser (payload: Object): Object {
  return {
    type: constants.USER_UPDATE_REQUEST,
    payload
  }
}


export function removeUser (id: string): Object {
  return {
    type: constants.USER_REMOVE_REQUEST,
    id
  }
}
