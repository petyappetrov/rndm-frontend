// @flow

import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { createLogger } from 'redux-logger'
import reducers from './reducers'
import sagas from './sagas'

const configureStore = (initialState: ?Object): Object => {
  const sagaMiddleware = createSagaMiddleware()
  const middlewares = [sagaMiddleware]

  if (process.env.NODE_ENV === 'development') {
    const logger = createLogger({
      collapsed: (getState, action, logEntry) => !logEntry.error
    })
    middlewares.push(logger)
  }

  const store = createStore(
    reducers,
    initialState,
    applyMiddleware(...middlewares)
  )

  sagaMiddleware.run(sagas)

  return store
}

export default configureStore
