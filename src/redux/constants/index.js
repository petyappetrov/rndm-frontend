import auth from './auth'
import notifications from './notifications'
import contests from './contests'
import users from './users'

export default {
  ...auth,
  ...notifications,
  ...contests,
  ...users
}
