import keyMirror from 'keymirror'

export default keyMirror({

  USER_GET_REQUEST: null,
  USER_GET_SUCCESS: null,
  USER_GET_FAILURE: null,

  USER_UPDATE_REQUEST: null,
  USER_UPDATE_SUCCESS: null,
  USER_UPDATE_FAILURE: null,

  USER_REMOVE_REQUEST: null,
  USER_REMOVE_SUCCESS: null,
  USER_REMOVE_FAILURE: null,

})
