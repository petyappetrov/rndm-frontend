// @flow

import constants from 'redux/constants'


const initialState = {
  isAuthenticated: null,
  isLoading: false,
  error: null,
  id: null
}


function auth (state: Object = initialState, action: Object) {
  switch (action.type) {
    case constants.AUTH_SIGNIN_REQUEST:
    case constants.AUTH_SIGNUP_REQUEST: {
      return {
        ...state,
        isLoading: true
      }
    }
    case constants.AUTH_SIGNIN_SUCCESS:
    case constants.AUTH_FROM_SOCIAL_NETWORK_SUCCESS: {
      return {
        ...state,
        isAuthenticated: true,
        isLoading: false,
        token: action.token,
        id: action.id
      }
    }
    case constants.AUTH_SIGNUP_SUCCESS: {
      return {
        ...state,
        isAuthenticated: false,
        isLoading: false,
        token: action.token,
      }
    }
    case constants.AUTH_SIGNIN_FAILURE: {
      return {
        ...state,
        isLoading: false,
        error: action.error
      }
    }
    case constants.AUTH_SIGNOUT_SUCCESS: {
      return {
        ...state,
        id: null,
        isAuthenticated: false
      }
    }
    default: {
      return state
    }
  }
}

export default auth
