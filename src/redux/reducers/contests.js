
import { combineReducers } from 'redux'
import { has, uniq, filter, omit } from 'lodash'
import constants from 'redux/constants'


function byId (state = {}, action): Object {
  switch (action.type) {
    case constants.CONTEST_CREATE_SUCCESS:
    case constants.CONTEST_GET_SUCCESS:
    case constants.CONTESTS_GET_SUCCESS:
    case constants.CONTESTS_USER_GET_SUCCESS: {
      return {
        ...state,
        ...action.entities.contests
      }
    }
    case constants.CONTEST_REMOVE__SUCCESS: {
      return omit(state, action.id)
    }
    default: {
      return state
    }
  }
}


function allIds (state = [], action): Array<string> {
  switch (action.type) {
    case constants.CONTEST_CREATE_SUCCESS:
    case constants.CONTEST_GET_SUCCESS:
    case constants.CONTESTS_GET_SUCCESS:
    case constants.CONTESTS_USER_GET_SUCCESS: {
      return uniq([
        ...state,
        ...action.result.contests
      ])
    }
    case constants.CONTEST_REMOVE__SUCCESS: {
      return filter(state, id => id !== action.id)
    }
    default: {
      return state
    }
  }
}


function filteredIds (state = [], action) {
  switch (action.type) {
    case constants.CONTESTS_GET_SUCCESS: {
      if (!action.filters) {
        return []
      }
      return action.result.contests
    }
    case constants.CONTEST_REMOVE__SUCCESS: {
      return filter(state, id => id !== action.id)
    }
    default: {
      return state
    }
  }
}


function isLoading (state = false, action) {
  switch (action.type) {
    case constants.CONTESTS_GET_REQUEST: {
      return true
    }
    case constants.CONTESTS_GET_SUCCESS: {
      return false
    }
    default: {
      return state
    }
  }
}

function count (state = 0, action) {
  if (has(action, 'result.count')) {
    return action.result.count
  }
  return state
}


export default combineReducers({
  byId,
  allIds,
  isLoading,
  count,
  filteredIds
})
