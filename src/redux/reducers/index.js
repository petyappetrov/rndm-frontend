import { combineReducers } from 'redux'
import notifications from './notifications'
import auth from './auth'
import contests from './contests'
import users from './users'


export default combineReducers({
  auth,
  notifications,
  contests,
  users
})
