// @flow

import { filter } from 'lodash'
import constants from 'redux/constants'


type State = Array<{
  title: string,
  message?: string,
  dismiss?: number,
  id: string
}>


export default function (state: State = [], action: Object) {
  switch (action.type) {
    case constants.NOTIFICATION_ADD: {
      return [
        ...state,
        action.notification
      ]
    }
    case constants.NOTIFICATION_REMOVE: {
      return filter(state, notification =>
        notification.id !== action.id
      )
    }
    default: {
      return state
    }
  }
}
