// @flow

import { combineReducers } from 'redux'
import { uniq } from 'lodash'
import constants from 'redux/constants'


function byId (state = {}, action) {
  switch (action.type) {
    case constants.USER_GET_SUCCESS:
    case constants.USER_UPDATE_SUCCESS: {
      return {
        ...state,
        ...action.entities.users
      }
    }
    default: {
      return state
    }
  }
}


function allIds (state = [], action) {
  switch (action.type) {
    case constants.USER_GET_SUCCESS: {
      return uniq([
        ...state,
        action.result
      ])
    }
    default: {
      return state
    }
  }
}


function isLoading (state = false, action) {
  switch (action.type) {
    case constants.USER_GET_SUCCESS:
    case constants.USER_GET_FAILURE: {
      return false
    }
    case constants.USER_GET_REQUEST: {
      return true
    }
    default: {
      return state
    }
  }
}


export default combineReducers({
  byId,
  allIds,
  isLoading
})
