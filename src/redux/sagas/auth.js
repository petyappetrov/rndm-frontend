// @flow

import { get } from 'lodash'
import { put, call, fork, takeLatest } from 'redux-saga/effects'
import { normalize } from 'normalizr'
import schemas from 'redux/schemas'
import constants from 'redux/constants'
import { history, savePersistedState, removePersistedState } from 'utils'
import clientAPI from 'api/clientAPI'


function* signin ({ payload: { rememberMe, ...payload } }: Object): Object {
  try {
    const { data } = yield call(clientAPI.signin, payload)
    yield call(clientAPI.setToken, data)
    if (rememberMe) {
      yield call(savePersistedState, {
        auth: {
          ...data,
          isAuthenticated: true
        }
      })
    }
    yield put({
      type: constants.AUTH_SIGNIN_SUCCESS,
      ...data
    })
  } catch (error) {
    yield put({
      type: constants.AUTH_SIGNIN_FAILURE,
      error: get(error, 'response.data.error')
    })
  }
}


function* signout (): Object {
  try {
    yield put({
      type: constants.AUTH_SIGNOUT_SUCCESS
    })
    yield call(removePersistedState)
    yield call(history.push, '/signin')
  } catch (error) {
    yield put({
      type: constants.AUTH_SIGNOUT_FAILURE
    })
  }
}


function* signup ({ payload }: Object): Object {
  try {
    const { data } = yield call(clientAPI.signup, payload)
    yield put({
      type: constants.AUTH_SIGNUP_SUCCESS,
      ...normalize(data, schemas.user)
    })
    yield call(history.push, {
      pathname: '/signin',
      state: { email: data.email }
    })
  } catch (error) {
    yield put({
      type: constants.AUTH_SIGNOUT_FAILURE
    })
  }
}


function* signinFromSocialNetwork ({ rememberMe, token, id }) {
  yield call(clientAPI.setToken, token)
  if (rememberMe) {
    yield call(savePersistedState, {
      auth: {
        id,
        token,
        isAuthenticated: true
      }
    })
  }
  yield put({
    type: constants.AUTH_FROM_SOCIAL_NETWORK_SUCCESS,
    id,
    token
  })
}


function* watchSignout () {
  yield takeLatest(constants.AUTH_SIGNOUT_REQUEST, signout)
}


function* watchSignup () {
  yield takeLatest(constants.AUTH_SIGNUP_REQUEST, signup)
}


function* watchSignin () {
  yield takeLatest(constants.AUTH_SIGNIN_REQUEST, signin)
}


function* watchSigninFromSocialNetwork () {
  yield takeLatest(constants.AUTH_FROM_SOCIAL_NETWORK_REQUEST, signinFromSocialNetwork)
}


export default function* auth (): any {
  yield fork(watchSignin)
  yield fork(watchSignout)
  yield fork(watchSignup)
  yield fork(watchSigninFromSocialNetwork)
}
