// @flow

import { put, call, fork, takeLatest } from 'redux-saga/effects'
import constants from 'redux/constants'
import clientAPI from 'api/clientAPI'
import { normalize } from 'normalizr'
import { history } from 'utils'
import schemas from 'redux/schemas'


function* createContest (payload) {
  try {
    const { data } = yield call(clientAPI.contest.createContest, payload)
    yield put({
      type: constants.CONTEST_CREATE_SUCCESS,
      ...normalize(data, { contests: [schemas.contest] })
    })
    yield call(history.push, `/contests/${data.contests[0]._id}`)
  } catch (error) {
    yield put({
      type: constants.CONTEST_CREATE_FAILURE,
      error
    })
  }
}


function* getContests ({ filters }) {
  try {
    const { data } = yield call(clientAPI.contest.getContests, filters)
    yield put({
      type: constants.CONTESTS_GET_SUCCESS,
      filters,
      ...normalize(data, { contests: [schemas.contest] })
    })
  } catch (error) {
    yield put({
      type: constants.CONTESTS_GET_FAILURE,
      error
    })
  }
}


function* getContest ({ id }) {
  try {
    const { data } = yield call(clientAPI.contest.getContests, { id })
    yield put({
      type: constants.CONTEST_GET_SUCCESS,
      ...normalize(data, { contests: [schemas.contest] })
    })
  } catch (error) {
    yield put({
      type: constants.CONTEST_GET_FAILURE,
      error
    })
  }
}


function* getUserContests ({ payload }) {
  try {
    const { data } = yield call(clientAPI.contest.getContests, payload)
    yield put({
      type: constants.CONTESTS_USER_GET_SUCCESS,
      ...normalize(data, { contests: [schemas.contest] })
    })
  } catch (error) {
    yield put({
      type: constants.CONTESTS_USER_GET_FAILURE,
      error
    })
  }
}


function* removeContest ({ id }) {
  try {
    const { data } = yield call(clientAPI.contest.removeContest, id)
    if (data.status !== 'success') {
      throw new Error('Error')
    }
    yield put({
      type: constants.CONTEST_REMOVE__SUCCESS,
      id
    })
    yield call(history.push, '/contests')
  } catch (error) {
    yield put({
      type: constants.CONTEST_REMOVE__FAILURE,
      error
    })
  }
}


function* watchCreateContest () {
  yield takeLatest(constants.CONTEST_CREATE_REQUEST, createContest)
}


function* watchGetContests () {
  yield takeLatest(constants.CONTESTS_GET_REQUEST, getContests)
}


function* watchGetContest () {
  yield takeLatest(constants.CONTEST_GET_REQUEST, getContest)
}


function* watchGetUserContests () {
  yield takeLatest(constants.CONTESTS_USER_GET_REQUEST, getUserContests)
}


function* watchRemoveContest () {
  yield takeLatest(constants.CONTEST_REMOVE__REQUEST, removeContest)
}


export default function* contests (): any {
  yield fork(watchCreateContest)
  yield fork(watchRemoveContest)
  yield fork(watchGetContests)
  yield fork(watchGetContest)
  yield fork(watchGetUserContests)
}
