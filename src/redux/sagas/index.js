import { fork } from 'redux-saga/effects'
import auth from './auth'
import notifications from './notifications'
import contests from './contests'
import users from './users'

export default function* sagas () {
  yield fork(auth)
  yield fork(notifications)
  yield fork(contests)
  yield fork(users)
}
