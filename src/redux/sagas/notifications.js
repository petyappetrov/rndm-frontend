// @flow

import { delay } from 'redux-saga'
import { put, call, takeEvery } from 'redux-saga/effects'
import constants from 'redux/constants'
import { hideNotification, showNotification } from 'redux/actions/notifications'


function* watchNotifications ({ notification }: {
  notification: {
    id: string,
    dismiss?: boolean
  }
}) {
  if (notification.dismiss) {
    yield call(delay, notification.dismiss)
    yield put(hideNotification(notification.id))
  }
}


function* signinMessage () {
  const message = {
    title: 'Добро пожаловать!',
    message: 'Вы успешно вошли в систему',
    dismiss: 6000
  }
  yield put(showNotification(message))
}


function* signoutMessage () {
  const message = {
    title: 'До скорой встречи!',
    message: 'Вы успешно вышли',
    dismiss: 6000
  }
  yield put(showNotification(message))
}


function* signupMessage ({ entities, result }) {
  const message = {
    title: `Поздавляем ${entities.users[result].name}, вы успешно зарегистрировались!`,
    message: 'Вы можете выполнить вход',
    dismiss: 6000
  }
  yield put(showNotification(message))
}


function* updateUserMessage () {
  const message = {
    title: 'Изменение профиля успешно выполнено',
    dismiss: 6000
  }
  yield put(showNotification(message))
}


function* removeUserMessage () {
  const message = {
    title: 'До свидания!',
    message: 'Ваш профиль удален',
    dismiss: 6000
  }
  yield put(showNotification(message))
}


function* afterCreatedConstestMessage () {
  const message = {
    title: 'Поздавляем с созданием конкурса!',
    message: 'Ваш конкурс успешно опубликован на сайте',
    dismiss: 6000
  }
  yield put(showNotification(message))
}


function* afterRemovedConstestMessage () {
  const message = {
    title: 'Конкурс успешно удален',
    dismiss: 6000
  }
  yield put(showNotification(message))
}


export default function* notifications (): Object {
  yield takeEvery(constants.NOTIFICATION_ADD, watchNotifications)

  yield takeEvery(constants.AUTH_SIGNIN_SUCCESS, signinMessage)
  yield takeEvery(constants.AUTH_SIGNOUT_SUCCESS, signoutMessage)
  yield takeEvery(constants.AUTH_SIGNUP_SUCCESS, signupMessage)


  yield takeEvery(constants.USER_UPDATE_SUCCESS, updateUserMessage)
  yield takeEvery(constants.USER_REMOVE_SUCCESS, removeUserMessage)

  yield takeEvery(constants.CONTEST_CREATE_SUCCESS, afterCreatedConstestMessage)
  yield takeEvery(constants.CONTEST_REMOVE__SUCCESS, afterRemovedConstestMessage)
}
