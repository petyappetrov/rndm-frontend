// @flow

import { takeLatest, put, call, fork } from 'redux-saga/effects'
import { normalize } from 'normalizr'
import clientAPI from 'api/clientAPI'
import constants from 'redux/constants'
import schemas from 'redux/schemas'
import { history } from 'utils'


function* getUser ({ id }) {
  try {
    const { data } = yield call(clientAPI.user.getUser, id)
    yield put({
      type: constants.USER_GET_SUCCESS,
      ...normalize(data, schemas.user)
    })
    yield put({
      type: constants.CONTESTS_USER_GET_REQUEST,
      payload: {
        createdBy: data._id,
        limit: 100
      }
    })
  } catch (error) {
    yield put({
      type: constants.USER_GET_FAILURE,
      error
    })
  }
}


function* updateUser ({ payload }) {
  try {
    const { data } = yield call(clientAPI.user.updateUser, payload)
    yield put({
      type: constants.USER_UPDATE_SUCCESS,
      ...normalize(data, schemas.user)
    })
    yield call(history.push, `/users/${data._id}`)
  } catch (error) {
    yield put({
      type: constants.USER_UPDATE_FAILURE,
      error
    })
  }
}


function* watchGetUser () {
  yield takeLatest(constants.USER_GET_REQUEST, getUser)
}


function* watchUpdateUser () {
  yield takeLatest(constants.USER_UPDATE_REQUEST, updateUser)
}


export default function* users (): Object {
  yield fork(watchGetUser)
  yield fork(watchUpdateUser)
}
