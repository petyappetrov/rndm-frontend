import { schema } from 'normalizr'


/**
 * Normalize schema for contests
 */
const contest = new schema.Entity('contests', {}, { idAttribute: '_id' })


/**
* Normalize schema for users
*/
const user = new schema.Entity('users', {}, { idAttribute: '_id' })


export default {
  contest,
  user
}
