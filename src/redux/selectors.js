// @flow

import { createSelector } from 'reselect'
import { filter, map } from 'lodash'


const contests = (state: Object) => state.contests
const auth = (state: Object) => state.auth


export const selectFilteredContests = createSelector(
  [
    state => state.contests.filteredIds,
    state => state.contests.byId
  ],
  (ids, byId) => map(ids, id => byId[id])
)


export const selectContest = (state: Object, props: Object) =>
  state.contests.byId[props.match.params.id] || {}


export const selectIsOwner = createSelector(
  [
    selectContest,
    auth
  ],
  (contest, auth) => contest.createdBy === auth.id
)


export const selectUser = (state: Object, props: Object) =>
  state.users.byId[props.match.params.id] || {}


export const selectUserContests = createSelector(
  [
    selectUser,
    contests
  ],
  (user, contests) => filter(contests.byId, contest => contest.createdBy === user._id)
)


export const selectIsMe = createSelector(
  [
    selectUser,
    auth
  ],
  (user, auth) => user._id === auth.id
)
