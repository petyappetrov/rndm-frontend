const express = require('express')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const path = require('path')
const compression = require('compression')
const config = require('../webpack/config.dev')

const app = express()


app.use(compression())
app.use(express.static(path.join(__dirname, '..', 'dist')))


if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(config)
  app.use(webpackDevMiddleware(compiler, {
    hot: true,
    stats: {
      colors: true,
      hash: false,
      timings: false,
      chunks: false,
      chunkModules: false,
      modules: false,
    }
  }))
  app.use(webpackHotMiddleware(compiler))
}


app.get('*.js', (req, res, next) => {
  req.url = `${req.url}.gz`
  res.set('Content-Encoding', 'gzip')
  next()
})


app.get('*', (req, res) =>
  res.sendFile(path.join(__dirname, '..', 'dist/index.html'))
)


app.listen(process.env.PORT, (err) => {
  if (err) {
    throw new Error(err)
  }
  console.log(`Server started on http://localhost:${process.env.PORT}`)
})
