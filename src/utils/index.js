
import { isNil, isString } from 'lodash'
import { createBrowserHistory } from 'history'
import withQuery from './withQuery'
import withWindowWidth from './withWindowWidth'

export function testConfirmPassword (value: string): boolean {
  return this.parent.password === value
}


export const history = createBrowserHistory()


export const bindActionToPromise = (dispatch, actionCreator) => (payload = {}) =>
  new Promise((resolve, reject) => dispatch(actionCreator({ payload, resolve, reject })))


export const API_URL = `${process.env.API_HOST}:${process.env.API_PORT}`


export const loadPersistedState = () => {
  try {
    const state: ?Object = localStorage.getItem('state')
    if (isNil(state)) {
      return undefined
    }
    return JSON.parse(state)
  } catch (error) {
    return undefined
  }
}


export const savePersistedState = (state: Object) => {
  try {
    localStorage.setItem('state', JSON.stringify(state))
  } catch (error) {
    console.error(error)
  }
}


export const removePersistedState = () => {
  try {
    const state = localStorage.getItem('state')
    if (!isNil(state)) {
      localStorage.removeItem('state')
    }
  } catch (error) {
    console.error(error)
  }
}


export const sliceString = (label, limit) => {
  if (!isString(label)) {
    return ''
  }
  if (label.length > limit) {
    return `${label.slice(0, limit)}...`
  }
  return label
}


export const possibleFilters = [
  'city',
  'type',
  'socialNetwork',
  'condition',
  'sortField',
  'sortDirection',
  'page',
  'search'
]


export {
  withQuery,
  withWindowWidth
}
