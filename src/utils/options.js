export const type = [
  {
    label: 'Все типы конкурсов',
    value: null
  },
  {
    label: 'По репосту',
    value: 'repost'
  },
  {
    label: 'По комментариям',
    value: 'comment'
  }
]


export const find = [
  {
    label: 'Случайно (Автоматически)',
    value: 'randomFromAuto'
  },
  {
    label: 'Случайно (По кнопке)',
    value: 'randomFromButton'
  },
  {
    label: 'Вручную огранизатором',
    value: 'manual',
  }
]


export const city = [
  {
    label: 'Все города',
    value: null
  },
  {
    label: 'Москва',
    value: 'msk'
  },
  {
    label: 'Санкт-Петербург',
    value: 'spb'
  },
  {
    label: 'Якутск',
    value: 'ykt',
  }
]


export const socialNetwork = [
  {
    label: 'Все социальные сети',
    value: null
  },
  {
    label: 'Инстаграм',
    value: 'instagram'
  },
  {
    label: 'Твиттер',
    value: 'twitter'
  }
]


export const sortFields = [
  {
    label: 'По дате завершения',
    value: 'endDate'
  },
  {
    label: 'По дате публикации',
    value: 'createdAt'
  },
  {
    label: 'По названию',
    value: 'title'
  }
]


export const sortDirections = [
  {
    label: 'По возрастанию',
    value: 'asc'
  },
  {
    label: 'По убыванию',
    value: 'desc'
  }
]


export const condition = [
  {
    label: 'Только активные',
    value: null
  },
  {
    label: 'Только завершенные',
    value: 'inactive'
  }
]
