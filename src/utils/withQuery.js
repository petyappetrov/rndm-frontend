import React from 'react'
import queryString from 'query-string'
import { isNil, omitBy } from 'lodash'

export default (WrappedComponent) =>
  class HandleQueries extends React.Component {
    updateQuery = (values) => {
      const { location, history } = this.props
      const query = queryString.parse(location.search)
      history.push({
        search: queryString.stringify(omitBy({ ...query, ...values }, isNil)),
        state: {
          jumpToTop: false
        }
      })
    }
    render () {
      return (
        <WrappedComponent
          {...this.props}
          query={queryString.parse(this.props.location.search)}
          updateQuery={this.updateQuery}
        />
      )
    }
  }
