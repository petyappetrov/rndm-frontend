import React from 'react'
import { debounce } from 'lodash'

const DEBOUNCE_MS = 200

export default (WrappedComponent) =>
  class WindowWidth extends React.Component {
    state = {
      width: 0
    }

    componentWillMount () {
      window.addEventListener('resize', this.listenerResize)
      this.handleResize()
    }

    componentWillUnmount () {
      window.removeEventListener('resize', this.listenerResize)
    }

    listenerResize = debounce(() =>
      this.handleResize(), DEBOUNCE_MS
    )

    handleResize = () => {
      if (this.state.width !== window.innerWidth) {
        this.setState({ width: window.innerWidth })
      }
    }

    render () {
      return (
        <WrappedComponent
          {...this.props}
          width={this.state.width}
        />
      )
    }
  }
