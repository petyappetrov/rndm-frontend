const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackTemplate = require('html-webpack-template')

module.exports = {
  output: {
    filename: 'js/app-[hash].js',
    path: path.resolve(__dirname, '../dist'),
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: [
          'babel-loader',
          // 'stylelint-custom-processor-loader' TODO: Enable in production config
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.svg$/,
        loader: 'svg-sprite-loader?name=img/[name]-[hash].[ext]'
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/,
        use: [
          {
            loader: 'file-loader?name=img/[name]-[hash].[ext]',
          }
        ]
      }
    ],
  },

  plugins: [
    new webpack.EnvironmentPlugin([
      'NODE_ENV',
      'API_PORT',
      'API_HOST'
    ]),
    new HtmlWebpackPlugin({
      inject: false,
      template: HtmlWebpackTemplate,
      title: 'Все конкурсы здесь',
      appMountId: 'root',
      alwaysWriteToDisk: true,
      mobile: true,
      lang: 'ru',
      links: [
        'https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic'
      ],
      scripts: [
        'https://use.fontawesome.com/04c83f925b.js'
      ]
    }),
  ],

  resolve: {
    extensions: ['.js', '.json'],
    modules: [
      'node_modules',
      path.join(__dirname, '../src')
    ]
  }
}
