const webpack = require('webpack')
const merge = require('webpack-merge')
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin')
const baseConfig = require('./config.base')

module.exports = merge(baseConfig, {
  devtool: 'eval-source-map',
  entry: [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client?path=/__webpack_hmr',
    './src/client.js'
  ],
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
    ]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new HtmlWebpackHarddiskPlugin()
  ]
})
